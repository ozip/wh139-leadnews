package com.heima.common.autoconfig.properties;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.awt.image.BufferedImage;

@Data
@ConfigurationProperties(prefix = "tess4j")
public class Tess4jPreperties {

    private String dataPath;
    private String language;

}