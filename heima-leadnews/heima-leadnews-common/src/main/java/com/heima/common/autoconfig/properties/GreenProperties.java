package com.heima.common.autoconfig.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "aliyun")
public class GreenProperties {

    private String accessKeyId;
    private String secret;
    private String scenes;

}
