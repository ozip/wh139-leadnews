package com.heima.common.autoconfig;

import com.heima.common.autoconfig.properties.GreenProperties;
import com.heima.common.autoconfig.properties.MinIOProperties;
import com.heima.common.autoconfig.properties.Tess4jPreperties;
import com.heima.common.autoconfig.template.GreenTemplate;
import com.heima.common.autoconfig.template.MinIOTemplate;
import com.heima.common.autoconfig.template.Tess4jTemplate;
import com.heima.common.redis.CacheService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;


@EnableConfigurationProperties({
        MinIOProperties.class,
        GreenProperties.class,
        Tess4jPreperties.class})
public class LeadNewsAutoConfig {

    @Bean
    @ConditionalOnProperty(prefix = "minio", value = "enable", havingValue = "true")
    public MinIOTemplate minIOTemplate(MinIOProperties minIOProperties) {
        return new MinIOTemplate(minIOProperties);
    }

    @Bean
    public GreenTemplate greenTemplate(GreenProperties greenProperties) {
        return new GreenTemplate(greenProperties);
    }

    @Bean
    public Tess4jTemplate tess4jTemplate(Tess4jPreperties tess4jPreperties) {
        return new Tess4jTemplate(tess4jPreperties);
    }

    @Bean
    public CacheService cacheService() {
        return new CacheService();
    }
}