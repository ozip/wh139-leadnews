package cn.itcast.mongo.test;

import com.itheima.mongo.demo.MongoApplication;
import com.itheima.mongo.demo.pojo.Person;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MongoApplication.class)
public class MongoTest {

    /**
     * SpringData-mongodb操作
     * 1 配置实体类
     * 2 实体类上配置注解（配置集合和对象间的映射关系）
     * 3 注入MongoTemplate对象
     * 4 调用对象方法，完成数据库操作
     */
    @Autowired
    private MongoTemplate mongoTemplate;

    //保存
    @Test
    public void testSave() {
        for (int i = 0; i < 10; i++) {
            Person person = new Person();
            person.setId(ObjectId.get()); //ObjectId.get()：获取一个唯一主键字符串
            person.setName("张三" + i);
            person.setAddress("武汉传智1" + i);
            person.setAge(18 + i);

            mongoTemplate.save(person);
        }
    }

    //查询-查询所有
    @Test
    public void testFindAll() {
        List<Person> list = mongoTemplate.findAll(Person.class);
        for (Person person : list) {
            System.out.println(person);
        }
    }

    @Test
    public void testFind() {
        //查询年龄小于20的所有人
        //Criteria criteria = Criteria.where("age").lt(20);
        Criteria criteria = Criteria.where("age").lt(20)
                .and("name").is("张三0");

        Query query = new Query(criteria);//查询条件对象
        //查询
        List<Person> list = mongoTemplate.find(query, Person.class);

        for (Person person : list) {
            System.out.println(person);
        }
    }

    /**
     * 分页查询
     */
    @Test
    public void testPage() {
        int page = 3;
        int size = 3;

        Criteria criteria = Criteria.where("age").lt(30);
        //1 查询总数
        Query queryCount = new Query(criteria);
        long count = mongoTemplate.count(queryCount, Person.class);
        System.out.println(count);
        //2 查询当前页的数据列表, 查询第二页，每页查询2条
        Query query = new Query(criteria)
                .limit(size)//设置每页查询条数
                .skip((page - 1) * size) //开启查询的条数
                .with(Sort.by(Sort.Order.asc("age")));

        List<Person> list = mongoTemplate.find(query, Person.class);
        for (Person person : list) {
            System.out.println(person);
        }
    }

    /**
     * 更新:
     * 根据id，更新年龄
     */
    @Test
    public void testUpdate() {
        //1 条件
        Query query = Query.query(Criteria.where("id").is("6194dff6652d843ef7feb148"));
        //2 更新的数据
        Update update = new Update();
        update.set("age", 12);
        
        mongoTemplate.updateFirst(query, update, Person.class);
    }

    @Test
    public void testRemove() {
        Query query = Query.query(Criteria.where("id").is("6194dff6652d843ef7feb148"));
        mongoTemplate.remove(query, Person.class);
    }
}