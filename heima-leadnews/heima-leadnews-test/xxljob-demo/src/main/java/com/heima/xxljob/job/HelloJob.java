package com.heima.xxljob.job;

import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import com.xxl.job.core.util.DateUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class HelloJob {

    @XxlJob("demoJobHandler")
    public void startJob() {
        //System.out.println(DateUtil.format(new Date(), "HH:mm:ss") + "，任务执行了");

        //当前我的分片索引
        int shardIndex = XxlJobHelper.getShardIndex();
        //分片总数
        int shardTotal = XxlJobHelper.getShardTotal();


        //System.out.println("当前分片总数是：" + shardTotal);
        //System.out.println("当前分片索引是：" + shardIndex);

        //
        //for (int i = 0; i < jobList.size(); i++) {
        //    //0,1,2,3,4  给0号分片
        //    if (i <= 4 && shardIndex == 0) {
        //        System.out.println(jobList.get(i));
        //    } else if (i > 4 && shardIndex == 1) {
        //        //5,6,7,8,9  给1号分片
        //        System.out.println(jobList.get(i));
        //    }
        //}

        for (int i = 0; i < jobList.size(); i++) {
            if (i % shardTotal == shardIndex) {
                System.out.println("当前分片是：" + shardIndex + ",执行任务" + jobList.get(i));
            }
        }

    }


    //模拟任务队列，RabbitMQ   Redis延时任务 。。。
    private List<String> jobList = new ArrayList<>();

    {
        for (int i = 0; i < 10; i++) {
            jobList.add("优惠券发给用户：" + i);
        }
    }
}
