package com.heima.kafka.stream;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.ValueMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Arrays;

@Component
public class KafkaStreamDemo {

    /**
     * 进行流式计算的逻辑
     * 统计单词出现的频率
     * hello kafka          hello itcast
     */
    @Bean
    public KStream<String, String> kStream(StreamsBuilder streamsBuilder) {
        //构建KStream对象，获取topic中的消息内容
        KStream<String, String> kStream = streamsBuilder.stream("heima-topic");

        //对value进行处理
        kStream.flatMapValues(new ValueMapper<String, Iterable<String>>() {
                    @Override
                    public Iterable<String> apply(String value) {
                        //hello kafka
                        return Arrays.asList(value.split(" "));
                    }
                })
                //按照value进行分组统计
                .groupBy((key, value) -> value)

                //设置时间窗口
                .windowedBy(TimeWindows.of(Duration.ofSeconds(10)))

                //数量统计
                .count()

                //转为Stream流
                .toStream()
                .map((key, value) -> {
                    System.out.println("key:" + key + ";  value:" + value);
                    return new KeyValue<>(key.toString(), value.toString());
                })

                //最终的结果发消息
                .to("heima-topic-stream");

        //返回KStream
        return kStream;
    }
}
