package com.heima.kafka.sample;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.ValueMapper;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;

//流式计算入门案例
public class KafkaStreamQuickStart {

    public static void main(String[] args) {
        //kafka的配置信息
        Properties prop = new Properties();
        prop.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.200.128:9092");
        prop.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        prop.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        prop.put(StreamsConfig.APPLICATION_ID_CONFIG, "streams-quickstart");

        //创建Stream构造器
        StreamsBuilder streamsBuilder = new StreamsBuilder();

        //进行流式计算
        streamProcessor(streamsBuilder);

        //创建KStream流式对象
        KafkaStreams kafkaStreams = new KafkaStreams(streamsBuilder.build(), prop);

        //开启流式计算
        kafkaStreams.start();

    }

    /**
     * 进行流式计算的逻辑
     * 统计单词出现的频率
     * hello kafka          hello itcast
     */
    private static void streamProcessor(StreamsBuilder streamsBuilder) {
        //获取KStream对象，从指定的topic中获取消息
        KStream<String, String> kStream = streamsBuilder.stream("heima-topic");

        //对value进行处理    要对单词进行数量统计
        kStream.flatMapValues(new ValueMapper<String, Iterable<String>>() {
                    @Override
                    public Iterable<String> apply(String value) {
                        //hello kafka  进行处理
                        return Arrays.asList(value.split(" "));
                    }
                })
                //进行统计分析，根据value进行分析
                //对value进行分组统计
                .groupBy((key, value) -> value)
                //设置时间窗口
                .windowedBy(TimeWindows.of(Duration.ofSeconds(10L)))
                //统计单词出现的次数
                .count()
                //转为Stream流
                .toStream()
                .map((key, value) -> {
                    System.out.println("key:" + key + ";  value:" + value);
                    return new KeyValue<>(key.toString(), value.toString());
                })
                //发消息
                .to("heima-topic-stream");

    }
}
