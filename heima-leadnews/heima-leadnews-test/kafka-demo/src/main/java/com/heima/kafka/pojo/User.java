package com.heima.kafka.pojo;

import lombok.Data;

@Data
public class User {
    private String name;
    private int age;
}
