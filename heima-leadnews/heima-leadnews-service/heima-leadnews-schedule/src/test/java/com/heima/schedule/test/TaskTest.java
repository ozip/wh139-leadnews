package com.heima.schedule.test;

import com.heima.model.schedule.dtos.Task;
import com.heima.schedule.ScheduleApplication;
import com.heima.schedule.service.TaskService;
import org.apache.commons.lang3.time.DateUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.charset.StandardCharsets;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ScheduleApplication.class)
public class TaskTest {

    @Autowired
    private TaskService taskService;

    @Test
    public void addTask() {
        Task task = new Task();
        task.setTaskType(100);
        task.setPriority(50);
        task.setParameters("task test".getBytes(StandardCharsets.UTF_8));
        //task.setExecuteTime(new Date().getTime());
        task.setExecuteTime(DateUtils.addMinutes(new Date(), 1).getTime());

        long id = taskService.addTask(task);
        System.out.println(id);
    }

    @Test
    public void cancelTask() {
        taskService.cancelTask(1520076684420792322L);
    }

    @Test
    public void pollTask() {
        taskService.pollTask(100, 50);
    }
}
