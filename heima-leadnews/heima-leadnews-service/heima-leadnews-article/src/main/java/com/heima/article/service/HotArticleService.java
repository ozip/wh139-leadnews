package com.heima.article.service;

import com.heima.model.mess.ArticleVisitStreamMess;

public interface HotArticleService {

    //计算热点文章
    public void computeHotArticle();

    //更新热点文章数据
    public void updateHotArticle(ArticleVisitStreamMess mess);
}