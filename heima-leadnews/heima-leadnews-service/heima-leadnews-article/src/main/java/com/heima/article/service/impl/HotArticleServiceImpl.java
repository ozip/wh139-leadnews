package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.apis.wemedia.IWemediaClient;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.HotArticleService;
import com.heima.common.constants.ArticleConstants;
import com.heima.common.constants.BehaviorConstants;
import com.heima.common.redis.CacheService;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.vos.HotArticleVo;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.mess.ArticleVisitStreamMess;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.utils.common.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HotArticleServiceImpl implements HotArticleService {

    @Autowired
    private ApArticleMapper apArticleMapper;
    @Autowired
    private CacheService cacheService;
    @Autowired
    private IWemediaClient wemediaClient;

    //计算文章的热点分值
    @Override
    public void computeHotArticle() {
        //1. 查询最近5天的文章
        //5天前的时间
        Date dateParam = DateUtils.addDay(new Date(), -5);
        //根据时间查询文章列表
        List<ApArticle> articleList = apArticleMapper.findByDays(dateParam);

        //2. 遍历所有的文章，计算热点分值    计算的原始数据是在redis
        List<HotArticleVo> hotArticleVoList = computeHotScore(articleList);

        //3. 对文章按照热点分值进行排序，并且把结果放到redis中
        addArticle2Redis(hotArticleVoList);
    }

    //更新热点文章数据
    @Override
    public void updateHotArticle(ArticleVisitStreamMess mess) {
        //查询当前文章数据
        ApArticle article = apArticleMapper.selectById(mess.getArticleId());

        //更新  每个频道的热点文章数据
        //获取redis中的当前频道的热点文章数据
        String channelJson = cacheService.get(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + article.getChannelId());
        List<HotArticleVo> channelVos = JSON.parseArray(channelJson, HotArticleVo.class);
        //更新Redis中的热点数据
        channelVos = replaceHotArticle(channelVos, mess, article);
        //放到redis中 频道数据里
        cacheService.set(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + article.getChannelId()
                , JSON.toJSONString(channelVos));


        //更新  推荐热点文章数据
        String allJson = cacheService.get(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + ArticleConstants.DEFAULT_TAG);
        List<HotArticleVo> allVos = JSON.parseArray(allJson, HotArticleVo.class);
        //更新Redis中的热点数据
        allVos = replaceHotArticle(allVos, mess, article);
        //放到redis中 频道数据里
        cacheService.set(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + ArticleConstants.DEFAULT_TAG
                , JSON.toJSONString(allVos));
    }

    //重新计算热点文章列表
    private List<HotArticleVo> replaceHotArticle(List<HotArticleVo> vos, ArticleVisitStreamMess mess, ApArticle article) {
        //当天的文章对用户比较重要，对当前的操作设置加权值   300% 就是乘以3被
        int sc = 3;

        //遍历热点文章数据
        for (HotArticleVo vo : vos) {
            //判断文章id是否一致
            if (vo.getId().longValue() == article.getId().longValue()) {
                //找到了当前文章对应的热点文章数据，直接累加热点分值即可
                vo.setScore(mess.getView() * 1 * sc + vo.getScore());
                vo.setScore(mess.getLike() * 3 * sc + vo.getScore());
                vo.setScore(mess.getComment() * 5 * sc + vo.getScore());
                vo.setScore(mess.getCollect() * 8 * sc + vo.getScore());

                //对文章进行重新排序，根据热点分值
                return vos.stream().sorted(Comparator.comparing(HotArticleVo::getScore).reversed())
                        .collect(Collectors.toList());
            }
        }

        //如果到这里，就表示当前的文章在原来的热点文章列表中，不存在
        //构建热点文章数据
        HotArticleVo vo = new HotArticleVo();
        BeanUtils.copyProperties(article, vo);
        vo.setScore(0);//设置为0分，初始值
        vo.setScore(mess.getView() * 1 * sc + vo.getScore());
        vo.setScore(mess.getLike() * 3 * sc + vo.getScore());
        vo.setScore(mess.getComment() * 5 * sc + vo.getScore());
        vo.setScore(mess.getCollect() * 8 * sc + vo.getScore());

        //如果当前的文章 在热点文章中  不存在  ，就需要比较热点分值
        //如果当前文章的热点分值  大于   当前热点分值最小的热点文章   替换热点文章
        if (vos.get(vos.size() - 1).getScore() < vo.getScore()) {
            //替换热点文章
            vos.set(vos.size() - 1, vo);
        }
        //对文章进行重新排序，根据热点分值
        return vos.stream().sorted(Comparator.comparing(HotArticleVo::getScore).reversed())
                .collect(Collectors.toList());
    }

    private void addArticle2Redis(List<HotArticleVo> hotArticleVoList) {
        //1 查询所有的频道
        ResponseResult responseResult = wemediaClient.getChannels();
        //判断查询结果收否有问题
        if (responseResult.getCode() != 200) {
            return;
        }

        //对查询结果进行处理，获取频道List集合   先把数据类型转为json  然后再把json转频道的List
        String json = JSON.toJSONString(responseResult.getData());
        List<WmChannel> channelList = JSON.parseArray(json, WmChannel.class);


        //2 遍历频道  对所有的频道都进行处理，获取频道内的热门文章
        for (WmChannel channel : channelList) {
            //3 获取频道内的文章，排序，获取10条最热门的文章
            List<HotArticleVo> channelVos = hotArticleVoList.stream()
                    .filter(hot -> hot.getChannelId().intValue() == channel.getId().intValue())//文章的过滤
                    .sorted(Comparator.comparing(HotArticleVo::getScore).reversed())//排序处理
                    .limit(10)
                    .collect(Collectors.toList());

            //放到redis中   频道数据里
            cacheService.set(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + channel.getId(), JSON.toJSONString(channelVos));

        }

        //4 对所有的热门文章进行排序，获取30条最热门的文章
        List<HotArticleVo> vos = hotArticleVoList.stream()
                .sorted(Comparator.comparing(HotArticleVo::getScore).reversed())//排序处理
                .limit(30)
                .collect(Collectors.toList());

        //放到redis中   频道数据里
        cacheService.set(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + ArticleConstants.DEFAULT_TAG, JSON.toJSONString(vos));
    }

    //遍历所有的文章，计算热点分值
    private List<HotArticleVo> computeHotScore(List<ApArticle> articleList) {

        List<HotArticleVo> vos = new ArrayList<>();

        for (ApArticle article : articleList) {
            HotArticleVo vo = new HotArticleVo();
            BeanUtils.copyProperties(article, vo);

            //计算分值
            int score = 0;

            //阅读权重：1    每次阅读，都会往redis中存一条阅读记录，hash结构，可以获取hash的数量，就代表阅读数
            Long readCount = cacheService.hSize(BehaviorConstants.READ_BEHAVIOR + article.getId().toString());
            if (readCount != null) {
                score += readCount.intValue() * 1;
            }

            //点赞权重：3
            Long likeCount = cacheService.hSize(BehaviorConstants.LIKE_BEHAVIOR + article.getId().toString());
            if (likeCount != null) {
                score += likeCount.intValue() * 3;
            }

            //评论权重：5
            //TODO  后期完成评论

            //收藏权重：8
            //TODO 需要给收藏功能进行完善，添加一个文章有多少用户收藏

            //设置分值
            vo.setScore(score);

            vos.add(vo);
        }

        return vos;
    }
}
