package com.heima.article.controller;

import com.heima.article.service.ArticleHomeService;
import com.heima.common.constants.ArticleConstants;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/article")
public class ArticleHomeController {


    @Autowired
    private ArticleHomeService apArticleHomeService;

    //文章的列表展示
    @PostMapping(value = {"/loadmore", "/loadnew"})
    public ResponseResult load(@RequestBody ArticleHomeDto dto) {
        return apArticleHomeService.load(dto);
    }

    //加载推荐文章
    @PostMapping(value = {"/load",})
    public ResponseResult load2(@RequestBody ArticleHomeDto dto) {
        return apArticleHomeService.load2(dto);
    }


    //修改或保存文章数据
    ///api/v1/article/save
    @PostMapping("/save")
    public ResponseResult save(@RequestBody ArticleDto dto) {
        return apArticleHomeService.saveArticle(dto);
    }
}