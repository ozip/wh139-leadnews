package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.dtos.ArticleDto;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.dtos.ArticleInfoDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;

import java.io.IOException;

public interface ArticleHomeService extends IService<ApArticle> {

    //文章的列表展示
    ResponseResult load(ArticleHomeDto dto);

    //保存文章数据
    ResponseResult saveArticle(ArticleDto dto);

    //加载文章详情 数据回显
    public ResponseResult loadArticleBehavior(ArticleInfoDto dto);

    //加载推荐文章
    ResponseResult load2(ArticleHomeDto dto);
}