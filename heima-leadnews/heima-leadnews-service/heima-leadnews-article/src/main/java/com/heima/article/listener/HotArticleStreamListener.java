package com.heima.article.listener;

import com.alibaba.fastjson.JSON;
import com.heima.article.service.HotArticleService;
import com.heima.common.constants.HotArticleConstants;
import com.heima.model.mess.ArticleVisitStreamMess;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class HotArticleStreamListener {

    @Autowired
    private HotArticleService hotArticleService;

    @KafkaListener(topics = HotArticleConstants.HOT_ARTICLE_INCR_HANDLE_TOPIC)
    public void onMessage(String msg) {
        //消息是否为空
        if (StringUtils.isBlank(msg)) {
            return;
        }

        //如果消息不为空，进行热点数据的实时更新
        ArticleVisitStreamMess mess = JSON.parseObject(msg, ArticleVisitStreamMess.class);
        hotArticleService.updateHotArticle(mess);

        System.out.println("热点文章数据的实时更新完成");
    }
}
