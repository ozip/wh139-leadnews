package com.heima.article.job;

import com.heima.article.service.HotArticleService;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HotArticleJob {

    @Autowired
    private HotArticleService hotArticleService;

    @XxlJob("computeHotArticleJob")
    public void hotArticle() {
        System.out.println("开始计算热门文章。。。。");
        hotArticleService.computeHotArticle();
        System.out.println("结束计算热门文章。。。。");
    }
}
