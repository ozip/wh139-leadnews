package com.heima.article.Stream;

import com.alibaba.fastjson.JSON;
import com.heima.common.constants.HotArticleConstants;
import com.heima.model.mess.ArticleVisitStreamMess;
import com.heima.model.mess.UpdateArticleMess;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class HotArticleStream {

    //构建KStream，进行流式处理
    @Bean
    public KStream<String, String> kStream(StreamsBuilder streamsBuilder) {
        //构建KStream对象，获取指定的topic中的消息数据
        KStream<String, String> kStream = streamsBuilder.stream(HotArticleConstants.HOT_ARTICLE_SCORE_TOPIC);

        //解析消息数据
        kStream.map(new KeyValueMapper<String, String, KeyValue<String, String>>() {
                    @Override
                    public KeyValue<String, String> apply(String key, String value) {
                        //进行消息的处理，获取到消息对象
                        UpdateArticleMess mess = JSON.parseObject(value, UpdateArticleMess.class);

                        //目的是计算文章的热点数据，所以可以按照文章的id进行分组处理
                        //把KStream 的   key：articleId(文章id)    value就是要分析的数据
                        //key:12223314234      value  VIEWS:2   对消息的内容进行重构
                        return new KeyValue<>(mess.getArticleId().toString(), mess.getType().name() + ":" + mess.getAdd());
                    }
                })
                //聚合操作，分组统计      根据文章id进行分组统计
                .groupBy((key, value) -> key)
                //设置时间窗口
                .windowedBy(TimeWindows.of(Duration.ofSeconds(10)))
                //进行流式计算中的   聚合操作
                .aggregate(new Initializer<String>() {
                    //初始化数据
                    @Override
                    public String apply() {
                        //流式计算之后，产生的完整的内容：
                        return "COLLECTION:0,COMMENT:0,LIKES:0,VIEWS:0";
                    }
                }, new Aggregator<String, String, String>() {
                    //真正的聚合处理，统计每个文章中  用户行为数量
                    @Override
                    public String apply(String key, String value, String aggValue) {
                        //分析value，根据value值进行结果的处理
                        // 如果传入的 VIEWS:2   那么最终聚合结果  COLLECTION:0,COMMENT:0,LIKES:0,VIEWS:0  -> COLLECTION:0,COMMENT:0,LIKES:0,VIEWS:2

                        //判断value不能为空，为空直接结束
                        if (StringUtils.isBlank(value)) {
                            return aggValue;
                        }

                        //COLLECTION:1,COMMENT:3,LIKES:5,VIEWS:2
                        String[] aggArr = aggValue.split(",");

                        //初始化互动数据
                        int col = 0, com = 0, lik = 0, vie = 0;

                        //如果不为空，需要对聚合结果进行解析  拿到单个的行为数据
                        for (String agg : aggArr) {
                            //COLLECTION:1
                            String[] split = agg.split(":");
                            switch (UpdateArticleMess.UpdateArticleType.valueOf(split[0])) {
                                case COLLECTION:
                                    col = Integer.parseInt(split[1]);
                                    break;
                                case COMMENT:
                                    com = Integer.parseInt(split[1]);
                                    break;
                                case LIKES:
                                    lik = Integer.parseInt(split[1]);
                                    break;
                                case VIEWS:
                                    vie = Integer.parseInt(split[1]);
                                    break;
                            }
                        }

                        //在对value进行解析  拿到单个的行为数据，和原来的数据进行累加
                        //value的值是：VIEWS:2   那么最终聚合结果  COLLECTION:0,COMMENT:0,LIKES:0,VIEWS:0  -> COLLECTION:0,COMMENT:0,LIKES:0,VIEWS:2
                        String[] valueArr = value.split(":");
                        switch (UpdateArticleMess.UpdateArticleType.valueOf(valueArr[0])) {
                            case COLLECTION:
                                col += Integer.parseInt(valueArr[1]);
                                break;
                            case COMMENT:
                                com += Integer.parseInt(valueArr[1]);
                                break;
                            case LIKES:
                                lik += Integer.parseInt(valueArr[1]);
                                break;
                            case VIEWS:
                                vie += Integer.parseInt(valueArr[1]);
                                break;
                        }

                        //把行为的结果，拼接为  COLLECTION:0,COMMENT:0,LIKES:0,VIEWS:0  字符串
                        String str = String.format("COLLECTION:%d,COMMENT:%d,LIKES:%d,VIEWS:%d", col, com, lik, vie);

                        System.out.println(key + "的文章，流式处理的结果是：" + str);

                        return str;
                    }
                }, Materialized.as("leadnew-hot-article-count-001"))//Store起名，保证唯一即可
                //把结果转为流
                .toStream()
                .map((key, value) -> {
                    //对发送到topic中的结果进行处理

                    //返回的消息，key就是文章id，原来的value是 COLLECTION:1,COMMENT:3,LIKES:5,VIEWS:2
                    return new KeyValue<>(key.key().toString(), formatObj(key.key().toString(), value));
                })

                //发消息
                .to(HotArticleConstants.HOT_ARTICLE_INCR_HANDLE_TOPIC);


        //返回结果
        return kStream;
    }

    //把COLLECTION:1,COMMENT:3,LIKES:5,VIEWS:2转为 对象  返回JSON数据
    private String formatObj(String articleId, String value) {
        //创建消息对象
        ArticleVisitStreamMess mess = new ArticleVisitStreamMess();
        mess.setArticleId(Long.parseLong(articleId));

        //COLLECTION:1,COMMENT:3,LIKES:5,VIEWS:2
        String[] valueArr = value.split(",");
        for (String val : valueArr) {
            //COLLECTION:1  ->   COLLECTION   1
            String[] split = val.split(":");
            switch (UpdateArticleMess.UpdateArticleType.valueOf(split[0])) {
                case COLLECTION:
                    mess.setCollect(Integer.parseInt(split[1]));
                    break;
                case COMMENT:
                    mess.setComment(Integer.parseInt(split[1]));
                    break;
                case LIKES:
                    mess.setLike(Integer.parseInt(split[1]));
                    break;
                case VIEWS:
                    mess.setView(Integer.parseInt(split[1]));
                    break;
            }
        }

        System.out.println("流式处理完成后，发送的消息内容是：" + JSON.toJSONString(mess));

        //返回JSON数据
        return JSON.toJSONString(mess);
    }
}
