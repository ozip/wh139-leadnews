package com.heima.minio.test;


import com.heima.article.ArticleApplication;
import com.heima.common.autoconfig.template.MinIOTemplate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest(classes = ArticleApplication.class)
public class MinioTest {

    @Autowired
    private MinIOTemplate minIOTemplate;

    @Test
    public void testUpdateImgFile() {
        try {
            FileInputStream fileInputStream = new FileInputStream("C:\\Users\\Tree-node2020\\Downloads\\d1cefe2e-f67c-4735-bc1c-0bedf14cd992.jpg");
            String filePath = minIOTemplate.uploadImgFile("", "tree1.jpg", fileInputStream);
            System.out.println(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}