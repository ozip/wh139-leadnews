package com.heima.user.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.user.dtos.LoginDto;
import com.heima.user.service.ApUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/login")
@Api(value = "app端用户登录", tags = "app端用户登录")
public class ApUserLoginController {

    @Value("${test.name}")
    private String testName;

    @Autowired
    private ApUserService apUserService;

    @PostMapping("/login_auth")
    @ApiOperation(value = "用户登录")
    public ResponseResult login(@RequestBody LoginDto dto) {
        System.out.println("当前的配置文件写的名字是：" + testName);
        return apUserService.login(dto);
    }
}