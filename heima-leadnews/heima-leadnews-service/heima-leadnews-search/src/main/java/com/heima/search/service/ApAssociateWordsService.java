package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;

//联想词
public interface ApAssociateWordsService {

    //联想词查询
    ResponseResult findAssociate(UserSearchDto userSearchDto);

}