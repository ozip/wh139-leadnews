package com.heima.wemedia;

import com.heima.common.autoconfig.template.GreenTemplate;
import com.heima.common.autoconfig.template.MinIOTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WemediaApplication.class)
public class GreenTest {

    @Autowired
    private GreenTemplate greenTemplate ;

    @Autowired
    private MinIOTemplate minIOTemplate;


    @Test
    public void testScanText() throws Exception {
        Map map = greenTemplate.greeTextScan("我是一个好人,冰毒");
        System.out.println(map);
    }

    @Test
    public void testScanImage() throws Exception {
        byte[] bytes = minIOTemplate.downLoadFile("http://192.168.200.128:9000/leadnews/2022/04/26/tree1.jpg");
        Map map = greenTemplate.imageScan(Arrays.asList(bytes));
        System.out.println(map);
    }
}