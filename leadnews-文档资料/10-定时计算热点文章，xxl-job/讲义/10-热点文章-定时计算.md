# xxl-Job分布式任务调度

## 1 功能开发分析

### 1.1 需求分析

![image-20210729224950851](热点文章-定时计算.assets\image-20210729224950851.png)

目前实现的思路：从数据库直接按照发布时间倒序查询

- 问题1：

  如何访问量较大，直接查询数据库，压力较大

- 问题2：

  新发布的文章会展示在前面，并不是热点文章



### 1.2 实现思路

把热点数据存入redis进行展示

判断文章是否是热点，有几项标准： 点赞数量，评论数量，阅读数量，收藏数量



计算文章热度，有两种方案：

- 定时计算文章热度

- 实时计算文章热度



### 1.3 定时计算

![image-20210729225206299](热点文章-定时计算.assets\image-20210729225206299.png)

- 根据文章的行为（点赞、评论、阅读、收藏）计算文章的分值，利用定时任务每天完成一次计算

- 把分值较大的文章数据存入到redis中

- App端用户查询文章列表的时候，优先从redis中查询热度较高的文章数据



### 1.4 定时任务框架-xxljob

spring传统的定时任务@Scheduled，但是这样存在这一些问题 ：

- 做集群任务的重复执行问题
- 定时任务失败了，无法重试也没有统计
- 如果任务量过大，不能有效的分片执行



解决这些问题的方案为：

xxl-job 分布式任务调度框架



## 2.分布式任务调度

### 2.1 什么是分布式任务调度

当前软件的架构已经开始向分布式架构转变，将单体结构拆分为若干服务，服务之间通过网络交互来完成业务处理。在分布式架构下，一个服务往往会部署多个实例来运行我们的业务，如果在这种分布式系统环境下运行任务调度，我们称之为**分布式任务调度**。

![image-20210729230059884](热点文章-定时计算.assets\image-20210729230059884.png)



将任务调度程序分布式构建，这样就可以具有分布式系统的特点，并且提高任务的调度处理能力：

1、并行任务调度

并行任务调度实现靠多线程，如果有大量任务需要调度，此时光靠多线程就会有瓶颈了，因为一台计算机CPU的处理能力是有限的。

如果将任务调度程序分布式部署，每个结点还可以部署为集群，这样就可以让多台计算机共同去完成任务调度，我们可以将任务分割为若干个分片，由不同的实例并行执行，来提高任务调度的处理效率。

2、高可用

若某一个实例宕机，不影响其他实例来执行任务。

3、弹性扩容

当集群中增加实例就可以提高并执行任务的处理效率。

4、任务管理与监测

对系统中存在的所有定时任务进行统一的管理及监测。让开发人员及运维人员能够时刻了解任务执行情况，从而做出快速的应急处理响应。



### 2.2 xxl-Job简介

针对分布式任务调度的需求，市场上出现了很多的产品：

| 名称        | 说明                                                         |
| ----------- | ------------------------------------------------------------ |
| TBSchedule  | 淘宝推出，多年未更新，文档缺失严重，缺少维护                 |
| Elastic-job | 当当网借鉴TBSchedule并基于quartz  二次开发的弹性分布式任务调度系统 |
| Saturn      | 唯品会开源的一个分布式任务调度平台，基于Elastic-job          |
| **XXL-Job** | 大众点评的分布式任务调度平台，是一个轻量级分布式任务调度平台，开发迅速、学习简单、轻量级、易扩展 |



XXL-JOB开箱即用。现已开放源代码并接入多家公司线上产品线

源码地址：https://gitee.com/xuxueli0323/xxl-job

文档地址：https://www.xuxueli.com/xxl-job/



**特性**

- **简单灵活**
  提供Web页面对任务进行管理，管理系统支持用户管理、权限控制；
  支持容器部署；
  支持通过通用HTTP提供跨平台任务调度；
- **丰富的任务管理功能**
  支持页面对任务CRUD操作；
  支持在页面编写脚本任务、命令行任务、Java代码任务并执行；
  支持任务级联编排，父任务执行结束后触发子任务执行；
  支持设置指定任务执行节点路由策略，包括轮询、随机、广播、故障转移、忙碌转移等；
  支持Cron方式、任务依赖、调度中心API接口方式触发任务执行
- **高性能**
  任务调度流程全异步化设计实现，如异步调度、异步运行、异步回调等，有效对密集调度进行流量削峰；
- **高可用**
  任务调度中心、任务执行节点均 集群部署，支持动态扩展、故障转移
  支持任务配置路由故障转移策略，执行器节点不可用是自动转移到其他节点执行
  支持任务超时控制、失败重试配置
  支持任务处理阻塞策略：调度当任务执行节点忙碌时来不及执行任务的处理策略，包括：串行、抛弃、覆盖策略
- **易于监控运维**
  支持设置任务失败邮件告警，预留接口支持短信、钉钉告警；
  支持实时查看任务执行运行数据统计图表、任务进度监控数据、任务完整执行日志；



### 2.3 配置部署调度中心-docker安装

1.创建mysql容器

```shell
docker run --name mysql57 \
-v /opt/mysql/conf:/etc/mysql \
-v /opt/mysql/logs:/var/log/mysql \
-v /opt/mysql/data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=root \
--restart=always  -p 3306:3306 \
-d mysql:5.7
```



2.初始化xxl-job的SQL脚本

​	连接上一步创建好的MySQL容器，创建xxl-job数据库，执行资料中的tables_xxl_job.sql脚本，最终效果如下：

<img src="images/image-20220504160539510.png" alt="image-20220504160539510" style="zoom:80%;" />



3.创建容器

```shell
docker run -d -e PARAMS="--spring.datasource.url=jdbc:mysql://192.168.200.128:3306/xxl_job?Unicode=true&characterEncoding=UTF-8 \
--spring.datasource.username=root \
--spring.datasource.password=root" \
--name xxl-job-admin \
-v /tmp:/data/applogs \
--restart=always -p 8888:8080 \
xuxueli/xxl-job-admin:2.3.0
```



访问地址：http://192.168.200.128:8888/xxl-job-admin

默认账号：admin

默认密码：123456



### 2.4 xxl-job入门案例编写

#### 2.4.1 新建任务

登录调度中心，修改自带的测试任务1，如下

<img src="热点文章-定时计算.assets\image-20210729232146585.png" alt="image-20210729232146585" style="zoom:80%;" />

<img src="images/image-20220504163155097.png" alt="image-20220504163155097" style="zoom:80%;" />



#### 2.4.2 入门案例

1. 在**heima-leadnews-test**模块中创建**xxljob-demo**，添加如下依赖

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <!--xxl-job-->
    <dependency>
        <groupId>com.xuxueli</groupId>
        <artifactId>xxl-job-core</artifactId>
        <version>2.3.0</version>
    </dependency>
</dependencies>
```



2. 在**xxljob-demo**中添加application.yml配置

```yaml
server:
  port: 8881
xxl:
  job:
    admin:
      addresses: http://192.168.200.128:8888/xxl-job-admin
    executor: 
      appname: xxl-job-executor-sample #执行器唯一标识
      port: 9999
```



3. 在**xxljob-demo**中编写引导类

```java
package com.heima.xxljob;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class XxlJobApplication {
    public static void main(String[] args) {
        SpringApplication.run(XxlJobApplication.class, args);
    }
}
```

4. 在**xxljob-demo**中新建配置类

```java
package com.heima.xxljob.config;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class XxlJobConfig {
    private Logger logger = LoggerFactory.getLogger(XxlJobConfig.class);

    @Value("${xxl.job.admin.addresses}")
    private String adminAddresses;

    @Value("${xxl.job.executor.appname}")
    private String appname;

    @Value("${xxl.job.executor.port}")
    private int port;


    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        logger.info("xxl-job config init.");
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(adminAddresses);
        xxlJobSpringExecutor.setAppname(appname);
        xxlJobSpringExecutor.setPort(port);
        return xxlJobSpringExecutor;
    }


}
```



5. 在**xxljob-demo**中编写任务代码

```java
package com.heima.xxljob.job;

import com.xxl.job.core.handler.annotation.XxlJob;
import org.springframework.stereotype.Component;

@Component
public class HelloJob {

    //注解的属性值，需要和xxl-job控制台配置的JobHandler一致
    @XxlJob("demoJobHandler")
    public void helloJob() {
        System.out.println(new Date() + "简单任务执行了。。。。");
    }
}
```

 

测试-单节点

- 启动微服务

- 在xxl-job的调度中心中启动任务，如下图

  ![image-20220504163831503](images/image-20220504163831503.png)



### 2.5 任务详解-执行器

- 执行器：任务的绑定的执行器，任务触发调度时将会自动发现注册成功的执行器, 实现任务自动发现功能; 

- 另一方面也可以方便的进行任务分组。每个任务必须绑定一个执行器

![image-20210729232926534](热点文章-定时计算.assets\image-20210729232926534.png)

![image-20210729232825564](热点文章-定时计算.assets\image-20210729232825564.png)

 以下是执行器的属性说明：

| 属性名称 | 说明                                                         |
| -------- | ------------------------------------------------------------ |
| AppName  | 是每个执行器集群的唯一标示AppName, 执行器会周期性以AppName为对象进行自动注册。可通过该配置自动发现注册成功的执行器, 供任务调度时使用; |
| 名称     | 执行器的名称, 因为AppName限制字母数字等组成,可读性不强, 名称为了提高执行器的可读性; |
| 排序     | 执行器的排序, 系统中需要执行器的地方,如任务新增, 将会按照该排序读取可用的执行器列表; |
| 注册方式 | 调度中心获取执行器地址的方式；                               |
| 机器地址 | 注册方式为"手动录入"时有效，支持人工维护执行器的地址信息；   |

自动注册和手动注册的区别和配置

![image-20210729233016355](热点文章-定时计算.assets\image-20210729233016355.png)

### 2.6 任务详解-基础配置

![image-20210729233926457](热点文章-定时计算.assets\image-20210729233926457.png)

**基础配置**

- 执行器：每个任务必须绑定一个执行器, 方便给任务进行分组

- 任务描述：任务的描述信息，便于任务管理；

- 负责人：任务的负责人；

- 报警邮件：任务调度失败时邮件通知的邮箱地址，支持配置多邮箱地址，配置多个邮箱地址时用逗号分隔

![image-20210729234009010](热点文章-定时计算.assets\image-20210729234009010.png)

**调度配置**

- 调度类型：
  - 无：该类型不会主动触发调度；
  - CRON：该类型将会通过CRON，触发任务调度；
  - 固定速度：该类型将会以固定速度，触发任务调度；按照固定的间隔时间，周期性触发；

![image-20210729234114283](热点文章-定时计算.assets\image-20210729234114283.png)

**任务配置**

- 运行模式：

  ​    BEAN模式：任务以JobHandler方式维护在执行器端；需要结合 "JobHandler" 属性匹配执行器中任务；

- JobHandler：运行模式为 "BEAN模式" 时生效，对应执行器中新开发的JobHandler类“@XxlJob”注解自定义的value值；

- 执行参数：任务执行所需的参数；

![image-20210729234219162](热点文章-定时计算.assets\image-20210729234219162.png)



*阻塞处理策略**

阻塞处理策略：调度过于密集执行器来不及处理时的处理策略；

- 单机串行（默认）：调度请求进入单机执行器后，调度请求进入FIFO(First Input First Output)队列并以串行方式运行；

- 丢弃后续调度：调度请求进入单机执行器后，发现执行器存在运行的调度任务，本次请求将会被丢弃并标记为失败；

- 覆盖之前调度：调度请求进入单机执行器后，发现执行器存在运行的调度任务，将会终止运行中的调度任务并清空队列，然后运行本地调度任务；

![image-20210729234256062](热点文章-定时计算.assets\image-20210729234256062.png)

**路由策略**

当执行器集群部署时，提供丰富的路由策略，包括；

- FIRST（第一个）：固定选择第一个机器；

- LAST（最后一个）：固定选择最后一个机器；

- **ROUND（轮询）**

- RANDOM（随机）：随机选择在线的机器；

- CONSISTENT_HASH（一致性HASH）：每个任务按照Hash算法固定选择某一台机器，且所有任务均匀散列在不同机器上。

- LEAST_FREQUENTLY_USED（最不经常使用）：使用频率最低的机器优先被选举；

- LEAST_RECENTLY_USED（最近最久未使用）：最久未使用的机器优先被选举；

- FAILOVER（故障转移）：按照顺序依次进行心跳检测，第一个心跳检测成功的机器选定为目标执行器并发起调度；

- BUSYOVER（忙碌转移）：按照顺序依次进行空闲检测，第一个空闲检测成功的机器选定为目标执行器并发起调度；

- **SHARDING_BROADCAST(分片广播)：广播触发对应集群中所有机器执行一次任务，同时系统自动传递分片参数；可根据分片参数开发分片任务；**

![image-20210729234409132](热点文章-定时计算.assets\image-20210729234409132.png)



### 2.7 路由策略(轮询)

1.修改任务为轮询

![image-20210729234513775](热点文章-定时计算.assets\image-20210729234513775.png)

2.启动多个微服务

​	需要修改端口号



### 2.8 路由策略(分片广播)

#### 2.8.1 分片逻辑

执行器集群部署时，任务路由策略选择”分片广播”情况下，一次任务调度将会广播触发对应集群中所有执行器执行一次任务

<img src="热点文章-定时计算.assets\image-20210729234756221.png" alt="image-20210729234756221" style="zoom:80%;" />

执行器集群部署时，任务路由策略选择”分片广播”情况下，一次任务调度将会广播触发对应集群中所有执行器执行一次任务

<img src="热点文章-定时计算.assets\image-20210729234822935.png" alt="image-20210729234822935" style="zoom:80%;" />



#### 2.8.2 路由策略

需求：让两个节点同时执行10000个任务，每个节点分别执行5000个任务

①：创建分片执行器

![image-20210729234930218](热点文章-定时计算.assets\image-20210729234930218.png)

②：创建任务，路由策略为分片广播

![image-20210729234948571](热点文章-定时计算.assets\image-20210729234948571.png)

③：分片广播代码

   分片参数

​     index：当前分片序号(从0开始)，执行器集群列表中当前执行器的序号；

​     total：总分片数，执行器集群的总机器数量；

修改yml配置

```yaml
server:
  port: 8881
xxl:
  job:
    admin:
      addresses: http://192.168.200.128:8888/xxl-job-admin
    executor:
#      appname: xxl-job-executor-sample #执行器名称
      appname: xxl-job-sharding-executor
      port: 9999
```

在HelloJob任务类中，添加任务执行的代码，如下：

```java
@XxlJob("shardingJobHandler")
public void helloJob2() {
    //获取分片总数
    int shardTotal = XxlJobHelper.getShardTotal();
    //获取当前分片序号
    int shardIndex = XxlJobHelper.getShardIndex();

    for (int i = 0; i < jobList.size(); i++) {
        if (i % shardTotal == shardIndex) {
            System.out.println("当前第" + shardIndex + "分片执行了，任务项为：" + jobList.get(i));
        }
    }
}

//模拟任务列表
private List<String> jobList = new ArrayList<>();
{
    for (int i = 0; i < 10; i++) {
        jobList.add("发送优惠券给用户" + i);
    }
}
```

测试

启动多个微服务测试，一次执行可以执行多个任务



## 3.热点文章-定时计算

### 3.1 需求分析

需求：为每个频道缓存热度较高的10条文章优先展示

![image-20210729235644605](热点文章-定时计算.assets\image-20210729235644605.png)

判断文章热度较高的标准是什么？

文章：阅读，点赞，评论，收藏



### 3.2 实现思路

![image-20210729235731309](热点文章-定时计算.assets\image-20210729235731309.png)

阅读权重：1

点赞权重：3

评论权重：5

收藏权重：8



### 3.3 实现步骤

分值计算不涉及到前端工程，也无需提供api接口，是一个纯后台的功能的开发。

#### 3.3.1 热文章业务层

在**heima-leadnews-model**中创建一个vo接收计算分值后的对象

```java
package com.heima.model.article.vos;

import com.heima.model.article.pojos.ApArticle;
import lombok.Data;

@Data
public class HotArticleVo extends ApArticle {
    //文章分值
    private Integer score;
}
```



在**heima-leadnews-article**中定义业务层接口

```java
package com.heima.article.service;

public interface HotArticleService {

    //计算热点文章
    public void computeHotArticle();
}
```

实现业务层接口类

```java
package com.heima.article.service.impl;

import com.alibaba.fastjson.JSON;
import com.heima.apis.wemedia.IWemediaClient;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.HotArticleService;
import com.heima.common.constants.ArticleConstants;
import com.heima.common.redis.CacheService;
import com.heima.model.article.dtos.HotArticleVo;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.wemedia.pojos.WmChannel;
import com.heima.utils.common.DateUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class HotArticleServiceImpl implements HotArticleService {

    @Autowired
    private ApArticleMapper apArticleMapper;
    @Autowired
    private IWemediaClient wemediaClient;
    @Autowired
    private CacheService cacheService;

    @Override
    public void computeHotArticle() {
        //1 查询最近5天的文章数据
        Date date = DateUtils.addDay(new Date(), -5);
        List<ApArticle> apArticleList = apArticleMapper.findByDays(date);

        //2 计算文章的分值
        List<HotArticleVo> hotArticleVos = computeScore(apArticleList);

        //3 为每个频道缓存10条分支较高的文章
        cacheToRedis(hotArticleVos);
    }

    //为每个频道缓存10条分值较高的文章
    private void cacheToRedis(List<HotArticleVo> hotArticleVos) {
        //查询频道
        ResponseResult responseResult = wemediaClient.getChannels();

        //判断查询结果是否不为200
        if (responseResult.getCode().intValue() != 200) {
            return;
        }

        //解析数据，获取频道
        String json = JSON.toJSONString(responseResult.getData());
        List<WmChannel> wmChannelList = JSON.parseArray(json, WmChannel.class);

        //检索每个频道的文章
        for (WmChannel wmChannel : wmChannelList) {
            List<HotArticleVo> channelVos = hotArticleVos.stream()
                    .filter(vo -> vo.getChannelId().intValue() == wmChannel.getId().intValue())
                    .sorted(Comparator.comparing(HotArticleVo::getScore).reversed())
                    .limit(10)
                    .collect(Collectors.toList());
            cacheService.set(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + wmChannel.getId(), JSON.toJSONString(channelVos));

        }

        //设置推荐数据
        //获取最高频率的文章并排序
        List<HotArticleVo> vos = hotArticleVos.stream()
                .sorted(Comparator.comparing(HotArticleVo::getScore).reversed())
                .limit(10)
                .collect(Collectors.toList());
        cacheService.set(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + ArticleConstants.DEFAULT_TAG, JSON.toJSONString(vos));

    }

    //计算文章分值
    private List<HotArticleVo> computeScore(List<ApArticle> apArticleList) {
        //创建容器
        List<HotArticleVo> vos = new ArrayList<>();
        for (ApArticle article : apArticleList) {
            //复制属性值
            HotArticleVo vo = new HotArticleVo();
            BeanUtils.copyProperties(article, vo);

            //计算分值
      	   int score = 0;
            //阅读数
            Long readCount = cacheService.hSize(BehaviorConstants.READ_BEHAVIOR
                    + article.getId().toString());
            if (readCount != null)
                score += readCount.intValue();

            //点赞数
            Long likeCount = cacheService.hSize(BehaviorConstants.LIKE_BEHAVIOR
                    + article.getId().toString());
            if (likeCount != null)
                score += likeCount.intValue() * 3;

            //TODO 评论

            //收藏数
            Long collectCount = cacheService.hSize(BehaviorConstants.COLLECTION_BEHAVIOR
                    + article.getId().toString());
            if (collectCount != null)
                score += collectCount.intValue() * 8;

            vo.setScore(score);
            vos.add(vo);
        }

        return vos;
    }
}
```



在**heima-leadnews-article**中的ArticleApplication的引导类中添加以下注解

```java
@EnableFeignClients(basePackages = "com.heima.apis")
```



在**heima-leadnews-article**中修改ApArticleMapper类，添加findArticleListByDays方法

```java
package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

@Mapper
public interface ApArticleMapper extends BaseMapper<ApArticle> {

    //加载文章列表 1 加载更多   2记载最新
    public List<ApArticle> loadArticleList(@Param("dto") ArticleHomeDto dto, @Param("type") Short type);

    //根据日期，查询最近的文章
    List<ApArticle> findByDays(@Param("dayParam") Date dayParam);
}
```

在**heima-leadnews-article**中的ApArticleMapper.xml新增sql配置

```xml
<select id="findByDays" resultMap="resultMap">
    SELECT
    aa.*
    FROM
    `ap_article` aa
    LEFT JOIN ap_article_config aac ON aa.id = aac.article_id
    <where>
        and aac.is_delete != 1
        and aac.is_down != 1
        <if test="dayParam != null">
            and aa.publish_time <![CDATA[>=]]> #{dayParam}
        </if>
    </where>
</select>
```



测试方法：

```java
package com.heima.article.service.impl;

import com.heima.article.ArticleApplication;
import com.heima.article.service.HotArticleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest(classes = ArticleApplication.class)
@RunWith(SpringRunner.class)
public class HotArticleServiceImplTest {

    @Autowired
    private HotArticleService hotArticleService;

    @Test
    public void computeHotArticle() {
        hotArticleService.computeHotArticle();
    }
}
```



#### 3.3.2 xxl-job定时计算-步骤

1. 在xxl-job-admin中新建执行器和任务

   新建热文章计算执行器：leadnews-hot-article-executor

![image-20210730000549587](热点文章-定时计算.assets\image-20210730000549587.png)



​		新建任务：路由策略为轮询，Cron表达式：0 0 2 * * ? 

<img src="热点文章-定时计算.assets\image-20210730000626824.png" alt="image-20210730000626824" style="zoom:80%;" />



2. 在**heima-leadnews-article**中的pom文件中新增依赖

```xml
<!--xxl-job-->
<dependency>
    <groupId>com.xuxueli</groupId>
    <artifactId>xxl-job-core</artifactId>
    <version>2.3.0</version>
</dependency>
```



3. 在**heima-leadnews-article**中集成xxl-job

   在nacos中的 leadnews-article配置中添加xxl-job配置，如下：

```properties
xxl:
  job:
    admin:
      addresses: http://192.168.200.128:8888/xxl-job-admin
    executor:
      appname: leadnews-hot-article-executor
      port: 9999
```



​	在**heima-leadnews-article**中创建定时任务配置类

```java
package com.heima.article.config;

import com.xxl.job.core.executor.impl.XxlJobSpringExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class XxlJobConfig {
    private Logger logger = LoggerFactory.getLogger(XxlJobConfig.class);

    @Value("${xxl.job.admin.addresses}")
    private String adminAddresses;

    @Value("${xxl.job.executor.appname}")
    private String appname;

    @Value("${xxl.job.executor.port}")
    private int port;


    @Bean
    public XxlJobSpringExecutor xxlJobExecutor() {
        logger.info("xxl-job config init.");
        XxlJobSpringExecutor xxlJobSpringExecutor = new XxlJobSpringExecutor();
        xxlJobSpringExecutor.setAdminAddresses(adminAddresses);
        xxlJobSpringExecutor.setAppname(appname);
        xxlJobSpringExecutor.setPort(port);
        return xxlJobSpringExecutor;
    }


}
```



4. 新建任务类

​	在**heima-leadnews-article**中添加任务类

```java
package com.heima.article.job;

import com.heima.article.service.HotArticleService;
import com.xxl.job.core.handler.annotation.XxlJob;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ComputeHotArticleJob {

    @Autowired
    private HotArticleService hotArticleService;

    @XxlJob("computeHotArticleJob")
    public void handle(){
        log.info("热文章分值计算调度任务开始执行...");
        hotArticleService.computeHotArticle();
        log.info("热文章分值计算调度任务结束...");

    }
}
```



## 4.查询文章接口改造

### 4.1 思路分析

![image-20210613110712894](热点文章-定时计算.assets\image-20210613110712894.png)

### 4.2 功能实现

在**heima-leadnews-article**中实现功能

#### 4.2.1 修改控制器

在**heima-leadnews-article**中修改ArticleHomeController中的load方法

```java
//文章的列表展示
@PostMapping(value = {"/loadmore", "/loadnew"})
public ResponseResult load(@RequestBody ArticleHomeDto dto) {
    return apArticleHomeService.load(dto);
}

//加载推荐文章列表
@PostMapping("/load")
public ResponseResult load2(@RequestBody ArticleHomeDto dto) {
    return apArticleHomeService.load2(dto);
}
```



#### 4.2.2 修改业务层

在**heima-leadnews-article**中的ApArticleService中新增接口

```java
//加载推荐文章列表
ResponseResult load2(ArticleHomeDto dto);
```

实现接口方法

```java
@Override
public ResponseResult load2(ArticleHomeDto dto) {
    String json = cacheService.get(ArticleConstants.HOT_ARTICLE_FIRST_PAGE + dto.getTag());
    if (StringUtils.isNotBlank(json)) {
        List<HotArticleVo> vos = JSON.parseArray(json, HotArticleVo.class);
        return ResponseResult.okResult(vos);
    }

    return load(dto);
}
```









