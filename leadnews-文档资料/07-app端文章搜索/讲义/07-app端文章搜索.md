# app端文章搜索

## 1 今日内容介绍

### 1.1 App端搜索-效果图

![image-20210709140539138](app端文章搜索.assets\image-20210709140539138.png)



## 2  搭建ElasticSearch环境

### 2.1 创建ElasticSearch容器

创建ElasticSearch服务

```sh
docker run -d  --name es --net=host \
-e "ES_JAVA_OPTS=-Xms512m -Xmx512m" \
-e "discovery.type=single-node" \
-v /usr/share/elasticsearch/plugins:/usr/share/elasticsearch/plugins \
--restart=always -p 9200:9200 -p 9300:9300 \
elasticsearch:7.4.0
```



### 2.2 创建kibana容器

```shell
docker run -d --name kibana --net=host \
-e ELASTICSEARCH_HOSTS=http://192.168.200.128:9200 \
--restart=always -p 5601:5601  \
kibana:7.4.0
```

访问地址：http://192.168.200.128:5601



### 2.3 配置中文分词器 ik

因为在创建elasticsearch容器的时候，映射了目录，所以可以在宿主机上进行配置ik中文分词器

在去选择ik分词器的时候，需要与elasticsearch的版本好对应上

把资料中的`ik`目录上传到服务器上,放到对应目录（plugins）

```shell
#切换目录
cd /usr/share/elasticsearch/plugins
```

<img src="images/image-20220502111622973.png" alt="image-20220502111622973" style="zoom:80%;" />



```sh
#上传ik成功后，需要重启容器
docker restart es
```



在kibana上测试：

```json
GET /_analyze
{
  "analyzer": "ik_max_word",
  "text": "学习黑马头条搜索功能"
}
```



## 3 app端文章搜索

### 3.1 需求分析

- 用户输入关键可搜索文章列表
- 关键词高亮显示
- 文章列表展示与home展示一样，当用户点击某一篇文章，可查看文章详情
- 文章排序是按照发布时间**倒序**排序

![image-20210709141502366](app端文章搜索.assets\image-20210709141502366.png)



### 3.2 思路分析

为了加快检索的效率，在查询的时候不会直接从数据库中查询文章，需要在elasticsearch中进行高速检索。

![image-20210709141558811](app端文章搜索.assets\image-20210709141558811.png)

### 3.3 创建索引和映射

![image-20220502115550900](images/image-20220502115550900.png)

添加映射

```json
PUT /app_info_article
{
    "mappings":{
        "properties":{
            "id":{
                "type":"long"
            },
            "publishTime":{
                "type":"date"
            },
            "layout":{
                "type":"integer"
            },
            "images":{
                "type":"keyword",
                "index": false
            },
            "staticUrl":{
                "type":"keyword",
                "index": false
            },
            "authorId": {
                "type": "long"
            },
            "authorName": {
                "type": "text"
            },
            "title":{
                "type":"text",
                "analyzer":"ik_smart",
        		"copy_to": "all"
            },
            "content":{
                "type":"text",
                "analyzer":"ik_smart",
        		"copy_to": "all"
            },
             "all":{
                "type": "text",
                "analyzer": "ik_smart"
             }
        }
    }
}
```

<img src="app端文章搜索.assets\1606653927638.png" alt="1606653927638" style="zoom:80%;" />



### 3.4 数据初始化到索引库

#### 3.4.1 导入工程

导入es-init到heima-leadnews-test工程下

![image-20210709142215818](app端文章搜索.assets\image-20210709142215818.png)

#### 3.4.2 批量导入文章到es索引库

```java
package com.heima.es;

import com.alibaba.fastjson.JSON;
import com.heima.es.mapper.ApArticleMapper;
import com.heima.es.pojo.SearchArticleVo;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@SpringBootTest
@RunWith(SpringRunner.class)
public class ApArticleTest {

    @Autowired
    private ApArticleMapper apArticleMapper;

    @Autowired
    private RestHighLevelClient restHighLevelClient;


    /**
     * 注意：数据量的导入，如果数据量过大，需要分页导入
     * @throws Exception
     */
    @Test
    public void init() throws Exception {

        //1.查询所有符合条件的文章数据
        List<SearchArticleVo> searchArticleVos = apArticleMapper.loadArticleList();

        //2.批量导入到es索引库

        BulkRequest bulkRequest = new BulkRequest("app_info_article");

        for (SearchArticleVo searchArticleVo : searchArticleVos) {

            IndexRequest indexRequest = new IndexRequest().id(searchArticleVo.getId().toString())
                    .source(JSON.toJSONString(searchArticleVo), XContentType.JSON);

            //批量添加数据
            bulkRequest.add(indexRequest);

        }
        restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);

    }

}
```



#### 3.4.3 测试

```json
GET /app_info_article/_search
```



### 3.5 文章搜索功能实现

#### 3.5.1 搭建搜索微服务

1. 导入资料中的heima-leadnews-search到工程中

![image-20210709142616797](app端文章搜索.assets\image-20210709142616797.png)



2. 在**heima-leadnews-search**的pom中添加依赖

```xml
<!--elasticsearch-->
<dependency>
    <groupId>org.elasticsearch.client</groupId>
    <artifactId>elasticsearch-rest-high-level-client</artifactId>
    <version>7.4.0</version>
</dependency>
<dependency>
    <groupId>org.elasticsearch.client</groupId>
    <artifactId>elasticsearch-rest-client</artifactId>
    <version>7.4.0</version>
</dependency>
<dependency>
    <groupId>org.elasticsearch</groupId>
    <artifactId>elasticsearch</artifactId>
    <version>7.4.0</version>
</dependency>
```



3. nacos配置中心添加leadnews-search配置，内容如下：

```yaml
spring:
  autoconfigure:
    exclude: org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
elasticsearch:
  host: 192.168.200.128
  port: 9200
```



4. nacos的leadnews-app-gateway网关配置中添加搜索微服务的路由配置

```yaml
        #搜索微服务
        - id: leadnews-search
          uri: lb://leadnews-search
          predicates:
            - Path=/search/**
          filters:
            - StripPrefix= 1
```

启动项目进行测试，至少要启动文章微服务，用户微服务，搜索微服务，app网关微服务，app前端工程





#### 3.5.2 搜索接口定义

|          | **说明**                      |
| -------- | ----------------------------- |
| 接口路径 | /api/v1/article/search/search |
| 请求方式 | POST                          |
| 参数     | UserSearchDto                 |
| 响应结果 | ResponseResult                |

注意响应结果  中的 title 是标题，h_title是标题的高亮字段。   返回的数据类型是List<Map>

```json
{
	"host": null,
	"code": 200,
	"errorMessage": "操作成功",
	"data": [
		{
			"layout": 1,
			"publishTime": 1599462132000,
			"images": "http://192.168.200.128/wKjlAc087.jpg",
			"authorName": "admin",
			"h_title": "我是一个<font style='color: red; font-size: inherit;'>测试</font>标题",
			"id": 1302864730402078722,
			"authorId": 4,
			"title": "我是一个测试标题",
			"content": [
				{
					"type": "text",
					"value": "这些都是测试这些都是测试这些都是"
				},
				{
					"type": "image",
					"value": "http://192.168.200.128/wKjlAc087.jpg"
				},
				{
					"type": "text",
					"value": "这些都是测试这些都是测试"
				}
			]
		},
		{...},
		{...}
	]
}
```



在**heima-leadnews-model**中的com.heima.model.search.dtos包内创建dto

```java
package com.heima.model.search.dtos;

import lombok.Data;
import java.util.Date;

@Data
public class UserSearchDto {

    //搜索关键字
    String searchWords;
    //当前页
    int pageNum;
    //分页条数
    int pageSize;
    //最小时间
    Date minBehotTime;

    public int getFromIndex() {
        if (this.pageSize < 1) this.pageSize = 10;
        if (this.pageNum < 1) return 0;
        return (this.pageNum - 1) * this.pageSize;
    }
}
```



在**heima-leadnews-search**中编写Controller

```java
package com.heima.search.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.search.service.ArticleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/article/search")
public class ArticleSearchController {

    @Autowired
    private ArticleSearchService articleSearchService;

    @PostMapping("/search")
    public ResponseResult search(@RequestBody UserSearchDto dto){
        return articleSearchService.search(dto);
    }
}
```



#### 3.5.3 业务层实现

在**heima-leadnews-search**创建业务层接口：ApArticleSearchService

```java
package com.heima.search.service;

import com.heima.model.search.dtos.UserSearchDto;
import com.heima.model.common.dtos.ResponseResult;

public interface ArticleSearchService {

    //ES文章分页搜索
    ResponseResult search(UserSearchDto dto);
}
```

实现接口：

```java
package com.heima.search.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.heima.common.thread.ApThreadLocal;
import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.search.service.ApUserSearchService;
import com.heima.search.service.ArticleSearchService;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ArticleSearchServiceImpl implements ArticleSearchService {

    @Autowired
    private RestHighLevelClient client;
    @Autowired
    private ApUserSearchService apUserSearchService;

    @SneakyThrows
    @Override
    public ResponseResult search(UserSearchDto dto) {
        //1 校验参数
        if (dto == null) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //2 设置查询条件
        //创建查询请求
        SearchRequest searchRequest = new SearchRequest("app_info_article");

        //布尔查询
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();

        //关键词
        if (StringUtils.isBlank(dto.getSearchWords())) {
            boolQueryBuilder.must(QueryBuilders.matchAllQuery());
        } else {
            boolQueryBuilder.must(QueryBuilders.matchQuery("all", dto.getSearchWords()));
        }

        searchRequest.source().query(boolQueryBuilder);

        //分页
        searchRequest.source().from(dto.getFromIndex());
        searchRequest.source().size(dto.getPageSize());

        //发布时间倒序
        searchRequest.source().sort("publishTime", SortOrder.DESC);

        //高亮  title
        searchRequest.source().highlighter(new HighlightBuilder()
                .field("title")
                .preTags("<font style='color: red; font-size: inherit;'>")
                .postTags("</font>")
                .requireFieldMatch(false)
        );

        //3 发起请求
        SearchResponse response = client.search(searchRequest, RequestOptions.DEFAULT);

        //4 解析请求
        List<Map> list = new ArrayList<>();

        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            Map map = JSON.parseObject(hit.getSourceAsString(), Map.class);

            //处理高亮
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            if (CollectionUtils.isNotEmpty(highlightFields)) {
                String title = highlightFields.get("title").getFragments()[0].toString();
                //高亮标题
                map.put("h_title", title);
            } else {
                //原始标题
                map.put("h_title", map.get("title"));
            }
            list.add(map);
        }
        return ResponseResult.okResult(list);
    }
}
```



### 3.6 文章自动审核

#### 3.6.1 思路分析

![image-20210709143151781](app端文章搜索.assets\image-20210709143151781.png)

#### 3.6.2 文章微服务发送消息

1. 在**heima-leadnews-model**中的com.heima.model.search.vos包内创建vo

把SearchArticleVo放到model工程下

```java
package com.heima.model.search.vos;

import lombok.Data;
import java.util.Date;

@Data
public class SearchArticleVo {

    // 文章id
    private Long id;
    // 文章标题
    private String title;
    // 文章发布时间
    private Date publishTime;
    // 文章布局
    private Integer layout;
    // 封面
    private String images;
    // 作者id
    private Long authorId;
    // 作者名词
    private String authorName;
    //静态url
    private String staticUrl;
    //文章内容
    private String content;
}
```



2. 在**heima-leadnews-article**文章微服务的ArticleFreemarkerService中的buildArticleToMinIO方法中**发送消息**

   修改的代码如下：

```java
@Async
@Override
public void buildArticleToMinIO(Long articleId, String content) {
    if (StringUtils.isNotBlank(content)) {
        //...其他代码...

        //发送消息，创建索引
        addArticleESIndex(articleId, content);
    }
}

@Autowired
private KafkaTemplate<String, String> kafkaTemplate;

//送消息，创建索引
private void addArticleESIndex(Long articleId, String content) {
    //查询文章数据
    ApArticle article = apArticleMapper.selectById(articleId);

    //创建vo并设置数据
    SearchArticleVo vo = new SearchArticleVo();
    BeanUtils.copyProperties(apArticle, vo);
    vo.setContent(content);

    kafkaTemplate.send(ArticleConstants.ARTICLE_ES_SYNC_TOPIC, JSON.toJSONString(vo));
}
```



3. 文章微服务集成kafka发送消息

   在nacos的leadnews-article文章服务配置中添加Kafka的配置，如下：（"注意删除多余的配置"）

```yaml
spring:
  kafka:
    bootstrap-servers: 192.168.200.128:9092
    producer:
      retries: 10
      key-serializer: org.apache.kafka.common.serialization.StringSerializer
      value-serializer: org.apache.kafka.common.serialization.StringSerializer
```



#### 3.6.3 搜索微服务接收消息

1. 在nacos的leadnews-search搜索微服务配置中添加kafka的配置，如下：

```yaml
spring:
  kafka:
    bootstrap-servers: 192.168.200.128:9092
    consumer:
      group-id: ${spring.application.name}
      key-deserializer: org.apache.kafka.common.serialization.StringDeserializer
      value-deserializer: org.apache.kafka.common.serialization.StringDeserializer
```



2. 在**heima-leadnews-search**服务中定义监听接收消息,保存索引数据

```java
package com.heima.search.listener;

import com.alibaba.fastjson.JSON;
import com.heima.common.constants.ArticleConstants;
import com.heima.model.search.vos.SearchArticleVo;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class ArticleListener {

    @Autowired
    private RestHighLevelClient client;

    @KafkaListener(topics = ArticleConstants.ARTICLE_ES_SYNC_TOPIC)
    public void onMessage(String message) {
        if (StringUtils.isNotBlank(message)) {
            SearchArticleVo vo = JSON.parseObject(message, SearchArticleVo.class);
            IndexRequest indexRequest = new IndexRequest("app_info_article")
                    .id(vo.getId().toString());
            indexRequest.source(message, XContentType.JSON);

            try {
                client.index(indexRequest, RequestOptions.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
```



## 4 MongoDB简介

对于某些软件功能功能，数据包含的特点如下：

- **数据量**会不断的**增大**
- 写操作的性能有要求
- 关键数据的读操作有性能要求
- **价值较低**
- **地理位置的查询**
- ……

针对以上特点，我们来分析一下：

* mysql：关系型数据库（效率低）MySQL8
* redis：redis缓存（微博，效率高，数据格式不丰富）

- 对于数据量大而言，进来不要使用关系型数据库进行存储，我们需要通过MongoDB进行存储
- 对于读多写少的应用，需要减少读取的成本
  - 比如说，一条SQL语句，单张表查询一定比多张表查询要快



* mongodb：存储业务数据（圈子，推荐的数据，小视频数据，点赞，评论等）
* redis：承担的角色是缓存层（提升查询效率）
* mysql：存储和核心业务数据，账户

MySQL和MongoDB存储数据最大的区别：数据价值



### 4.1 MongoDB简介

MongoDB：是一个高效的非关系型数据库（不支持表关系：只能操作单表）

![image-20201101152836048](images/image-20201101152836048.png)

MongoDB是一个基于分布式文件存储的数据库。由C++语言编写。旨在为WEB应用提供可扩展的高性能数据存储解决方案。

MongoDB是一个介于关系数据库和非关系数据库之间的产品，是非关系数据库当中功能最丰富，最像关系数据库的，它支持的数据结构非常松散，是类似json的bson格式，因此可以存储比较复杂的数据类型。

MongoDB最大的特点是它支持的**查询语言非常强大**，其语法有点类似于面向对象的查询语言，几乎可以实现类似关系数据库单表查询的绝大部分功能，而且还支持对数据建立索引。

官网：https://www.mongodb.com



### 4.2 MongoDB的特点

MongoDB 最大的特点是他支持的查询语言非常强大，其语法有点类似于面向对象的查询语言，几乎可以实现类似关系数据库单表查询的绝大部分功能，而且还支持对数据建立索引。它是一个面向集合的,模式自由的文档型数据库。具体特点总结如下： 

1. 面向集合存储，易于存储对象类型的数据 
2. 模式自由 
3. 支持动态查询 
4. 支持完全索引，包含内部对象 
5. 支持复制和故障恢复 
6. 使用高效的二进制数据存储，包括大型对象（如视频等） 
7. 自动处理碎片，以支持云计算层次的扩展性 
8. 支持 Python，PHP，Ruby，**Java**，C，C#，Javascript，Perl及C++语言的驱动程 序， 社区中也提供了对Erlang及.NET 等平台的驱动程序 
9. 文件存储格式为 BSON（一种 JSON 的扩展）

> MYSQL : 用于存储安全性要求比较高的数据
>
> REDIS : 存储数据格式简单 , 并且查询非常多的数据(用户缓存)
>
> MONGDB : 用户存储海量数据, 并且数据的安全性要求不高



#### 4.2.1 通过docker安装MongoDB

我们只需要使用简单的命令即可启动MongoDB

~~~shell
docker run -id --name mongo \
-v /opt/mongo/data:/data/db \
--restart=always -p 27017:27017 \
mongo:4.4.10
~~~



客户端解压，运行robo3t.exe即可使用：

![image-20220502124020915](images/image-20220502124020915.png)

<img src="images/image-20220502124307287.png" alt="image-20220502124307287" style="zoom:80%;" />



#### 4.2.2 MongoDB体系结构(PPT)

MongoDB 的逻辑结构是一种层次结构。主要由： 文档(document) 集合(collection) 数据库(database)这三部分组成的。逻辑结构是面 向用户的，用户使用 MongoDB 开发应用程序使用的就是逻辑结构。 

1. MongoDB 的文档（document），相当于关系数据库中的一行记录。 
2. 多个文档组成一个集合（collection），相当于关系数据库的表。 
3. 多个集合（collection），逻辑上组织在一起，就是数据库（database）。 
4. 一个 MongoDB 实例支持多个数据库（database）。 文档(document) 集合(collection) 数据库(database)的层次结构如下图:

<img src="images/image-20201101155443521.png" alt="image-20201101155443521" style="zoom:80%;" />



为了更好的理解，下面与SQL中的概念进行对比：

| SQL术语/概念 | MongoDB术语/概念 | 解释/说明                           |
| ------------ | ---------------- | ----------------------------------- |
| database     | database         | 数据库                              |
| table        | collection       | 数据库表/集合                       |
| row          | document         | 表中的一条数据                      |
| column       | field            | 数据字段/域                         |
| index        | index            | 索引                                |
| table joins  |                  | 表连接,MongoDB不支持                |
| primary key  | primary key      | 主键,MongoDB自动将_id字段设置为主键 |

 ![img](images/Figure-1-Mapping-Table-to-Collection-1.png)



#### 4.2.3 数据类型

* 数据格式：BSON  {aa:bb}

- null：用于表示空值或者不存在的字段，{“x”:null} 
- 布尔型：布尔类型有两个值true和false，{“x”:true} 
- 数值：shell默认使用64为浮点型数值。{“x”：3.14}或{“x”：3}。对于整型值，可以使用 NumberInt（4字节符号整数）或NumberLong（8字节符号整数）， {“x”:NumberInt(“3”)}{“x”:NumberLong(“3”)} 
- 字符串：UTF-8字符串都可以表示为字符串类型的数据，{“x”：“呵呵”} 
- 日期：日期被存储为自新纪元依赖经过的毫秒数，不存储时区，{“x”:new Date()}
- 正则表达式：查询时，使用正则表达式作为限定条件，语法与JavaScript的正则表达式相 同，{“x”:/[abc]/} 
- 数组：数据列表或数据集可以表示为数组，{“x”： [“a“，“b”,”c”]} 
- 内嵌文档：文档可以嵌套其他文档，被嵌套的文档作为值来处理，{“x”:{“y”:3 }} 
- 对象Id：对象id是一个12字节的字符串，是文档的唯一标识，{“x”: objectId() } 
- 二进制数据：二进制数据是一个任意字节的字符串。它不能直接在shell中使用。如果要 将非utf-字符保存到数据库中，二进制数据是唯一的方式。



## 5  MongoDB入门

### 5.1 数据库以及表的操作

~~~shell
#查看所有的数据库
> show databases
> show dbs

#通过use关键字切换数据库
> use db

#创建数据库
#说明：在MongoDB中，数据库是自动创建的，通过use切换到新数据库中，进行插入数据即可自动创建数据库
> use testdb

> show dbs #并没有创建数据库

> db.user.insert({id:1,name:'zhangsan'})  #插入数据

#查看表
> show tables

> show collections

#删除集合（表）
> db.user.drop()
true  #如果成功删除选定集合，则 drop() 方法返回 true，否则返回 false。

#删除数据库
> use testdb #先切换到要删除的数据中

> db.dropDatabase()  #删除数据库


~~~

### 5.2 新增数据

在MongoDB中，存储的文档结构是一种类似于json的结构，称之为bson（全称为：Binary JSON）。

~~~shell
#插入数据
#语法：db.表名.insert(json字符串)

> db.user.insert({id:1,username:'zhangsan',age:20})


> db.user.find()  #查询数据
~~~

### 5.3 更新数据

update() 方法用于更新已存在的文档。语法格式如下：

```shell
db.collection.update(
   <query>,
   <update>,
   [
     upsert: <boolean>,
     multi: <boolean>,
     writeConcern: <document>
   ]
)
```

**参数说明：**

- **query** : update的查询条件，类似sql update查询内where后面的。
- **update** : update的对象和一些更新的操作符（如$,$inc.$set）等，也可以理解为sql update查询内set后面的
- **upsert** : 可选，这个参数的意思是，如果不存在update的记录，是否插入objNew,true为插入，默认是false，不插入。
- **multi** : 可选，mongodb 默认是false,只更新找到的第一条记录，如果这个参数为true,就把按条件查出来多条记录全部更新。
- **writeConcern** :可选，抛出异常的级别。

~~~shell
#查询全部
> db.user.find()

#更新数据
> db.user.update({id:1},{$set:{age:22}}) 

#注意：如果这样写，会删除掉其他的字段
> db.user.update({id:1},{age:25})

#更新不存在的字段，会新增字段
> db.user.update({id:2},{$set:{sex:1}}) #更新数据

#更新不存在的数据，默认不会新增数据
> db.user.update({id:3},{$set:{sex:1}})

#如果设置第一个参数为true，就是新增数据
> db.user.update({id:3},{$set:{sex:1}},true)
~~~

### 5.4 删除数据

通过remove()方法进行删除数据，语法如下：

~~~shell
db.collection.remove(
   <query>,
   {
     justOne: <boolean>,
     writeConcern: <document>
   }
)
~~~

**参数说明：**

- **query** :（可选）删除的文档的条件。
- **justOne** : （可选）如果设为 true 或 1，则只删除一个文档，如果不设置该参数，或使用默认值 false，则删除所有匹配条件的文档。
- **writeConcern** :（可选）抛出异常的级别。

实例：

~~~shell
#删除数据
> db.user.remove({})

#插入4条测试数据
db.user.insert({id:1,username:'zhangsan',age:20})
db.user.insert({id:2,username:'lisi',age:21})
db.user.insert({id:3,username:'wangwu',age:22})
db.user.insert({id:4,username:'zhaoliu',age:22})

> db.user.remove({age:22},true)

#删除所有数据
> db.user.remove({})
~~~

### 5.5 查询数据

MongoDB 查询数据的语法格式如下：

```
db.user.find([query],[fields])
```

- **query** ：可选，使用查询操作符指定查询条件
- **fields** ：可选，使用投影操作符指定返回的键。查询时返回文档中所有键值， 只需省略该参数即可（默认省略）。

条件查询：

| 操作       | 格式                     | 范例                                        | RDBMS中的类似语句         |
| ---------- | ------------------------ | ------------------------------------------- | ------------------------- |
| 等于       | `{<key>:<value>`}        | `db.col.find({"by":"黑马程序员"}).pretty()` | `where by = '黑马程序员'` |
| 小于       | `{<key>:{$lt:<value>}}`  | `db.col.find({"likes":{$lt:50}}).pretty()`  | `where likes < 50`        |
| 小于或等于 | `{<key>:{$lte:<value>}}` | `db.col.find({"likes":{$lte:50}}).pretty()` | `where likes <= 50`       |
| 大于       | `{<key>:{$gt:<value>}}`  | `db.col.find({"likes":{$gt:50}}).pretty()`  | `where likes > 50`        |
| 大于或等于 | `{<key>:{$gte:<value>}}` | `db.col.find({"likes":{$gte:50}}).pretty()` | `where likes >= 50`       |
| 不等于     | `{<key>:{$ne:<value>}}`  | `db.col.find({"likes":{$ne:50}}).pretty()`  | `where likes != 50`       |

实例：

~~~shell
#插入测试数据
db.user.insert({id:1,username:'zhangsan',age:20})
db.user.insert({id:2,username:'lisi',age:21})
db.user.insert({id:3,username:'wangwu',age:22})
db.user.insert({id:4,username:'zhaoliu',age:22})

db.user.find()  #查询全部数据
db.user.find({},{id:1,username:1})  #只查询id与username字段
db.user.find().count()  #查询数据条数
db.user.find({id:1}) #查询id为1的数据
db.user.find({age:{$lte:21}}) #查询小于等于21的数据
db.user.find({$or:[{id:1},{id:2}]}) #查询id=1 or id=2

#分页查询：Skip()跳过几条，limit()查询条数
db.user.find().limit(2).skip(1)  #跳过1条数据，查询2条数据
db.user.find().sort({id:-1}) #按照id倒序排序，-1为倒序，1为正序
~~~



## 6 SpringData-Mongo

Spring-data对MongoDB做了支持，使用spring-data-mongodb可以简化MongoDB的操作，封装了底层的mongodb-driver。

地址：https://spring.io/projects/spring-data-mongodb

使用Spring-Data-MongoDB很简单，只需要如下几步即可：

* 导入起步依赖
* 编写配置信息
* 编写实体类（配置注解  @Document，@Id）
* 操作mongodb
  * **注入MongoTemplate对象，完成CRUD操作**
  * 编写Repository接口，注入接口完成基本Crud操作



### 6.1 环境搭建

在**heima-leadnews-test**中创建子模块**mongo-demo**

导入依赖：

~~~xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-data-mongodb</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
    </dependency>
    <dependency>
        <groupId>com.alibaba</groupId>
        <artifactId>fastjson</artifactId>
        <version>1.2.8</version>
    </dependency>
</dependencies>
~~~



编写application.yml配置文件

~~~properties
server:
  port: 9998
spring:
  data:
    mongodb:
      host: 192.168.200.128
      port: 27017
      database: leadnews-history
~~~



编写启动类

```java
package com.heima.mongo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MongoApplication.class, args);
    }
}
```



### 6.2 完成基本操作

1. 编写实体类

~~~java
package com.heima.mongo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(value="person")
public class Person {

    @Id
    private ObjectId id;
    @Field("username")
    private String name;
    private int age;
    private String address;

}
~~~

2. 通过MongoTemplate完成CRUD操作

   1. 保存
   2. 查询所有
   3. 根据年龄查询
   4. 分页查询
   5. 根据id更新年龄
   6. 删除



~~~java
package com.heima.mongo.test;

import com.itheima.mongo.demo.MongoApplication;
import com.itheima.mongo.demo.pojo.Person;
import org.bson.types.ObjectId;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.query.UpdateDefinition;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MongoApplication.class)
public class MongoTest {

    /**
     * SpringData-mongodb操作
     * 1 配置实体类
     * 2 实体类上配置注解（配置集合和对象间的映射关系）
     * 3 注入MongoTemplate对象
     * 4 调用对象方法，完成数据库操作
     */
    @Autowired
    private MongoTemplate mongoTemplate;


    //1. 保存
    @Test
    public void testSave() {
        for (int i = 0; i < 10; i++) {
            Person person = new Person();
            person.setName("张三");
            person.setAge(18 + i);
            person.setAddress("武汉黑马" + i);

            mongoTemplate.save(person);
        }
    }

    //2. 查询所有
    @Test
    public void testFindAll() {
        List<Person> personList = mongoTemplate.find(new Query(), Person.class);

        for (Person person : personList) {
            System.out.println(person);
        }
    }

    //3. 根据年龄查询
    @Test
    public void testFind() {
        //查询年龄小于20的所有人
        List<Person> personList = mongoTemplate.find(Query.query(
                Criteria.where("age").lt(20)
        ), Person.class);

        for (Person person : personList) {
            System.out.println(person);
        }
    }

    //4. 分页查询
    @Test
    public void testPage() {
        //查询年龄小于20的所有人
        Criteria criteria = Criteria.where("age").gte(20);
        Query query = Query.query(criteria);

        int page = 1;
        int size = 3;
        query.skip((page - 1) * size)
                .limit(size)
                .with(Sort.by(Sort.Order.desc("age")));


        List<Person> personList = mongoTemplate.find(query, Person.class);


        for (Person person : personList) {
            System.out.println(person);
        }

    }

    //5. 根据id更新年龄
    @Test
    public void testUpdate() {
        Query query = Query.query(Criteria.where("age").is(18));

        UpdateDefinition update = Update.update("name", "李四");
        mongoTemplate.updateFirst(query, update, Person.class);

    }

    //6. 删除
    @Test
    public void testRemove() {
        Criteria criteria = Criteria.where("id").is(new ObjectId("628e62cae97e996f4fd1ff85"));
        mongoTemplate.remove(Query.query(criteria), Person.class);
    }
}
~~~



## 7 app端搜索-搜索记录

### 7.1 需求分析

![1587366878895](app端文章搜索.assets\1587366878895.png)

- 展示用户的搜索记录10条，按照搜索关键词的时间倒序
- 可以删除搜索记录
- 保存历史记录，保存10条，多余的则删除最久的历史记录



### 7.2 数据存储说明

用户的搜索记录，需要给每一个用户都保存一份，数据量较大，要求加载速度快，通常这样的数据存储到mongodb更合适，不建议直接存储到关系型数据库中

![image-20210709153428259](app端文章搜索.assets\image-20210709153428259.png)

### 7.3 保存搜索记录

#### 7.3.1 实现思路

![image-20210709153935904](app端文章搜索.assets\image-20210709153935904.png)

用户输入关键字进行搜索的异步记录关键字

![image-20210709154053892](app端文章搜索.assets\image-20210709154053892.png)



#### 7.3.2 实现步骤

1.在**heima-leadnews-search**搜索微服务集成mongodb

​	添加依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-mongodb</artifactId>
</dependency>
```

​	在nacos的leadnews-search搜索配置中添加如下内容：

```yaml
spring:
  data:
   mongodb:
    host: 192.168.200.128
    port: 27017
    database: leadnews-history
```



2. 在**heima-leadnews-search**搜索微服务中创建实体类：

```java
package com.heima.search.pojos;

import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

//APP用户搜索信息表
@Data
@Document("ap_user_search")
public class ApUserSearch implements Serializable {
    //主键
    @Id
    private String id;
    //用户ID
    private Integer userId;
    //搜索词
    private String keyword;
    //创建时间
    private Date createdTime;
}
```



3. 在**heima-leadnews-search**搜索微服务中创建ApUserSearchService接口

```java
package com.heima.search.service;

public interface ApUserSearchService {

    //保存用户搜索历史记录
    void insert(String keyword,Integer userId);
}
```

实现接口：

```java
package com.heima.search.service.impl;

import com.heima.search.pojos.ApUserSearch;
import com.heima.search.service.ApUserSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ApUserSearchServiceImpl implements ApUserSearchService {

    @Autowired
    private MongoTemplate mongoTemplate;

    //保存用户搜索历史记录
    @Override
    public void insert(String keyword, Integer userId) {
        //1. 查询当前用户的搜索关键词
        ApUserSearch apUserSearch = mongoTemplate.findOne(Query.query(
            Criteria.where("userId").is(userId)
            .and("keyword").is(keyword)
        ), ApUserSearch.class);

        //2. 不为空是数据存在 更新创建时间
        if (apUserSearch != null) {
            apUserSearch.setCreatedTime(new Date());
            mongoTemplate.save(apUserSearch);
            return;
        }

        //3. 为空是数据不存在，查询历史记录，创建时间倒序排序
        List<ApUserSearch> list = mongoTemplate.find(Query.query(
            Criteria.where("userId")
        ).with(Sort.by(Sort.Order.desc("createdTime"))), ApUserSearch.class);

        //4. 判断是否超过10条
        apUserSearch = new ApUserSearch();
        apUserSearch.setUserId(userId);
        apUserSearch.setKeyword(keyword);
        apUserSearch.setCreatedTime(new Date());

        if (list.size() < 10) {
            //4.1 不超过10条，添加
            mongoTemplate.save(apUserSearch);
        } else {
            //4.2 达到10条，替换最后一个
            ApUserSearch apUserSearchLast = list.get(list.size() - 1);
            mongoTemplate.findAndReplace(Query.query(
                Criteria.where("id").is(apUserSearchLast.getId())
            ), apUserSearch);
        }
    }
}
```



3. 参考自媒体相关微服务，获取当前登录的用户

   在**heima-leadnews-common**模块中，创建使用ThreadLocal存储用户的工具类

```java
package com.heima.common.thread;

import com.heima.model.user.pojos.ApUser;

public class ApThreadLocal {

    private final static ThreadLocal<ApUser> AP_USER_THREAD_LOCAL = new ThreadLocal<>();

    //添加用户
    public static void setUser(ApUser apUser) {
        AP_USER_THREAD_LOCAL.set(apUser);
    }

    //获取用户
    public static ApUser getUser() {
        return AP_USER_THREAD_LOCAL.get();
    }

    //清理用户
    public static void clear() {
        AP_USER_THREAD_LOCAL.remove();
    }
}
```

​	在**heima-leadnews-search**搜索微服务中创建拦截器

```java
package com.heima.search.interceptor;

import com.heima.common.thread.ApThreadLocal;
import com.heima.model.user.pojos.ApUser;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class ApTokenInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //得到header中的信息
        String userId = request.getHeader("userId");
        if (StringUtils.isNotBlank(userId)) {
            //把用户id存入threadloacl中
            ApUser apUser = new ApUser();
            apUser.setId(Integer.valueOf(userId));
            ApThreadLocal.setUser(apUser);
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        ApThreadLocal.clear();
    }
}
```

配置拦截器

```java
package com.heima.search.config;

import com.heima.search.interceptor.ApTokenInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new ApTokenInterceptor()).addPathPatterns("/**");
    }
}
```



4. 修改**heima-leadnews-app-gateway**的AuthorizeFilter，添加token校验通过后，把用户id放到request中，如下：

```java
@Override
public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
    //...其他代码

    //5.判断token是否有效
    try {
        Claims claimsBody = AppJwtUtil.getClaimsBody(token);
        //是否是过期
        int result = AppJwtUtil.verifyToken(claimsBody);
        if (result == 1 || result == 2) {
            response.setStatusCode(HttpStatus.UNAUTHORIZED);
            return response.setComplete();
        }

        //获得token解析后中的用户信息
        String userId = claims.get("id").toString();
        //在header中添加新的信息
        ServerHttpRequest serverHttpRequest = request.mutate().headers(
            httpHeaders -> httpHeaders.set("userId", userId)
        ).build();
        //重置header
        exchange.mutate().request(serverHttpRequest).build();
        
    } catch (Exception e) {
        e.printStackTrace();
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        return response.setComplete();
    }

    //6.放行
    return chain.filter(exchange);
}
```



5. 在**heima-leadnews-search**的ArticleSearchService的search方法中调用保存历史记录

代码如下：

```java
@Autowired
private ApUserSearchService apUserSearchService;

//es文章分页检索
@Override
public ResponseResult search(UserSearchDto dto) throws IOException {

    //1.检查参数
    if (dto == null || StringUtils.isBlank(dto.getSearchWords())) {
        return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
    }

    ApUser user = ApThreadLocal.getUser();
    if (user != null) {
        apUserSearchService.insert(dto.getSearchWords(), user.getId());
    }

    //2.设置查询条件
    //...其他代码
}
```



### 7.4 加载搜索记录列表

#### 7.4.1 思路分析

按照当前用户，按照时间倒序查询

|          | **说明**             |
| -------- | -------------------- |
| 接口路径 | /api/v1/history/load |
| 请求方式 | POST                 |
| 参数     | 无                   |
| 响应结果 | ResponseResult       |



#### 7.4.2 接口定义

在**heima-leadnews-search**中编写Controller

```java
package com.heima.search.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.search.service.ApUserSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//APP用户搜索信息表 前端控制器
@RestController
@RequestMapping("/api/v1/history")
public class ApUserSearchController{

    @Autowired
    private ApUserSearchService apUserSearchService;

    //获取用户搜索记录
    @PostMapping("/load")
    public ResponseResult findUserSearch() {
        return apUserSearchService.findUserSearch();
    }

}
```



#### 7.4.3 业务层

在**heima-leadnews-search**中的ApUserSearchService中新增方法

```java
//查询搜索历史
ResponseResult findUserSearch();
```

实现方法

```java
//查询搜索历史
@Override
public ResponseResult findUserSearch() {
    //获取当前用户
    ApUser user = ApThreadLocal.getUser();
    if (user == null) {
        return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
    }

    //根据用户查询数据，按照时间倒序
    List<ApUserSearch> apUserSearches = mongoTemplate.find(
        Query.query(Criteria.where("userId").is(user.getId()))
        .with(Sort.by(Sort.Order.desc("createdTime"))),
        ApUserSearch.class);
    return ResponseResult.okResult(apUserSearches);
}
```



### 7.5 删除搜索记录

#### 7.5.1 思路分析

按照搜索历史id删除

|          | **说明**            |
| -------- | ------------------- |
| 接口路径 | /api/v1/history/del |
| 请求方式 | POST                |
| 参数     | HistorySearchDto    |
| 响应结果 | ResponseResult      |

在**heima-leadnews-model**中创建dto

```java
package com.heima.model.search.dtos;

import lombok.Data;

@Data
public class HistorySearchDto {
    //接收搜索历史记录id
    String id;
}
```



#### 7.5.2 接口定义

在**heima-leadnews-search**中的ApUserSearchController新增方法

```java
@PostMapping("/del")
public ResponseResult delUserSearch(@RequestBody HistorySearchDto historySearchDto) {
    return apUserSearchService.delUserSearch(historySearchDto);
}
```



#### 7.5.3 业务层

在**heima-leadnews-search**的ApUserSearchService中新增接口

```java
//删除搜索历史
ResponseResult delUserSearch(HistorySearchDto historySearchDto);
```

实现接口方法

```java
//删除历史记录
@Override
public ResponseResult delUserSearch(HistorySearchDto dto) {
    //1.检查参数
    if (dto.getId() == null) {
        return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
    }

    //2.判断是否登录
    ApUser user = ApThreadLocal.getUser();
    if (user == null) {
        return ResponseResult.errorResult(AppHttpCodeEnum.NEED_LOGIN);
    }

    //3.删除
    mongoTemplate.remove(Query.query(Criteria.where("id").is(dto.getId())), ApUserSearch.class);

    return ResponseResult.okResult(null);
}
```





## 8 app端搜索-关键字联想词

### 8.1 需求分析

![1587366921085](app端文章搜索.assets\1587366921085.png)

- 根据用户输入的关键字展示联想词



### 8.2 搜索词-数据来源

通常是网上搜索频率比较高的一些词，通常在企业中有两部分来源：

第一：自己维护搜索词

通过分析用户搜索频率较高的词，按照排名作为搜索词

第二：第三方获取

关键词规划师（百度）、5118、爱站网

![image-20210709160036983](app端文章搜索.assets\image-20210709160036983.png)

导入资料中的ap_associate_words.js脚本到mongo中



在**heima-leadnews-search**搜索微服务中创建联想词表实体类：

```java
package com.heima.search.pojos;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

//联想词表
@Data
@Document("ap_associate_words")
public class ApAssociateWords implements Serializable {
    //主键
    @Id
    private String id;
    //联想词
    private String associateWords;
    //创建时间
    private Date createdTime;
}
```



### 8.3 功能实现

#### 8.3.1 接口定义

|          | **说明**                 |
| -------- | ------------------------ |
| 接口路径 | /api/v1/associate/search |
| 请求方式 | POST                     |
| 参数     | UserSearchDto            |
| 响应结果 | ResponseResult           |



新建Controller

```java
package com.heima.search.controller;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.search.service.ApAssociateWordsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//联想词表 前端控制器
@RestController
@RequestMapping("/api/v1/associate")
public class ApAssociateWordsController{

    @Autowired
    private ApAssociateWordsService apAssociateWordsService;

    @PostMapping("/search")
    public ResponseResult findAssociate(@RequestBody UserSearchDto userSearchDto) {
        return apAssociateWordsService.findAssociate(userSearchDto);
    }
}
```



#### 8.3.2 业务层

新建联想词业务层接口

```java
package com.heima.search.service;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.search.dtos.UserSearchDto;

//联想词
public interface ApAssociateWordsService {

    //联想词查询
    ResponseResult findAssociate(UserSearchDto userSearchDto);

}
```

实现类

```java
package com.heima.search.service.impl;

import com.heima.model.common.dtos.ResponseResult;
import com.heima.model.common.enums.AppHttpCodeEnum;
import com.heima.model.search.dtos.UserSearchDto;
import com.heima.search.pojos.ApAssociateWords;
import com.heima.search.service.ApAssociateWordsService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApAssociateWordsServiceImpl implements ApAssociateWordsService {

    @Autowired
    MongoTemplate mongoTemplate;

    //联想词
    @Override
    public ResponseResult findAssociate(UserSearchDto userSearchDto) {
        //1 参数检查
        if (userSearchDto == null || StringUtils.isBlank(userSearchDto.getSearchWords())) {
            return ResponseResult.errorResult(AppHttpCodeEnum.PARAM_INVALID);
        }

        //3 执行查询 模糊查询
        Query query = Query.query(Criteria.where("associateWords").regex(".*?\\" + userSearchDto.getSearchWords() + ".*"));
        query.limit(userSearchDto.getPageSize());
        List<ApAssociateWords> wordsList = mongoTemplate.find(query, ApAssociateWords.class);

        return ResponseResult.okResult(wordsList);
    }
}
```



