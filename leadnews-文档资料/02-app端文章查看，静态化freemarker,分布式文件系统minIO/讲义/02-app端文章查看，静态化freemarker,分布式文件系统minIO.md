

# app端文章查看，静态化freemarker,分布式文件系统minIO

## 1 文章列表加载

### 1.1 需求分析

文章布局展示

![image-20210419151801252](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210419151801252.png)



### 1.2 表结构分析

创建数据库**leadnews_article**，执行  资料\sql脚本  目录中的  leadnews_article.sql  文件。



ap_article  文章基本信息表

![image-20210419151839634](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210419151839634.png)

ap_article_config  文章配置表

![image-20210419151854868](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210419151854868.png)

ap_article_content 文章内容表

![image-20210419151912063](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210419151912063.png)

三张表关系分析

![image-20210419151938103](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210419151938103.png)



**表的拆分 - 垂直分表**

垂直分表：将一个表的字段分散到多个表中，每个表存储其中一部分字段。

**拆分规则**：

​	1.把不常用的字段单独放在一张表

​	2.把text，blob等大字段拆分出来单独放在一张表

​	3.经常组合查询的字段单独放在一张表中

**优势**：

1. 减少IO争抢，减少锁表的几率，查看文章概述与文章详情互不影响

2. 充分发挥高频数据的操作效率，对文章概述数据操作的高效率不会被操作文章详情数据的低效率所拖累。



### 1.3 导入文章数据库

在heima-leadnews-model模块中，添加ap_article文章表对应实体类

```java
package com.heima.model.article.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 文章信息表，存储已发布的文章
 */

@Data
@TableName("ap_article")
public class ApArticle implements Serializable {

    @TableId(value = "id",type = IdType.ID_WORKER)
    private Long id;


    //标题
    private String title;

    //作者id
    @TableField("author_id")
    private Long authorId;

    //作者名称
    @TableField("author_name")
    private String authorName;

    //频道id
    @TableField("channel_id")
    private Integer channelId;

    //频道名称
    @TableField("channel_name")
    private String channelName;

    //文章布局  0 无图文章   1 单图文章    2 多图文章
    private Short layout;

    //文章标记  0 普通文章   1 热点文章   2 置顶文章   3 精品文章   4 大V 文章
    private Byte flag;

    //文章封面图片 多张逗号分隔
    private String images;

    //标签
    private String labels;

    //点赞数量
    private Integer likes;

    //收藏数量
    private Integer collection;

    //评论数量
    private Integer comment;

    //阅读数量
    private Integer views;

    //省市
    @TableField("province_id")
    private Integer provinceId;

    //市区
    @TableField("city_id")
    private Integer cityId;

    //区县
    @TableField("county_id")
    private Integer countyId;

    //创建时间
    @TableField("created_time")
    private Date createdTime;

    //发布时间
    @TableField("publish_time")
    private Date publishTime;

    //同步状态
    @TableField("sync_status")
    private Boolean syncStatus;

    //来源
    private Boolean origin;

    //静态页面地址
    @TableField("static_url")
    private String staticUrl;
}
```



在heima-leadnews-model模块中，添加ap_article_config文章配置对应实体类

```java
package com.heima.model.article.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * APP已发布文章配置表
 */

@Data
@TableName("ap_article_config")
public class ApArticleConfig implements Serializable {

    @TableId(value = "id",type = IdType.ID_WORKER)
    private Long id;

    //文章id
    @TableField("article_id")
    private Long articleId;

    //是否可评论 true:可以评论    false:不可评论  0
    @TableField("is_comment")
    private Boolean isComment;

    //是否转发 true:可以转发   false:不可转发  0
    @TableField("is_forward")
    private Boolean isForward;

    //是否下架 true:下架   false:没有下架  0
    @TableField("is_down")
    private Boolean isDown;

    //是否已删除  true:删除   false:没有删除  0
    @TableField("is_delete")
    private Boolean isDelete;
}
```



在heima-leadnews-model模块中，添加ap_article_content 文章内容对应的实体类

```java
package com.heima.model.article.pojos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

@Data
@TableName("ap_article_content")
public class ApArticleContent implements Serializable {

    @TableId(value = "id",type = IdType.ID_WORKER)
    private Long id;

    /**
     * 文章id
     */
    @TableField("article_id")
    private Long articleId;

    /**
     * 文章内容
     */
    private String content;
}
```



### 1.4 实现思路

<img src="app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210419152011931.png" alt="image-20210419152011931" style="zoom:80%;" />

1,在默认频道展示5条文章信息

2,可以切换频道查看不同种类文章

3,当用户下拉可以加载最新的文章（分页），本页文章列表中发布时间为最大的时间为依据

4,当用户上拉可以加载更多的文章信息（按照发布时间）本页文章列表中发布时间最小的时间为依据

5，前端传递默认参数：

- maxBehotTime：当前文章列表中，最新的文章的发布时间
- minBehotTime： 当前文章列表中，最老的文章的发布时间



sql语句：

```sql
# 按照发布时间倒序查询10条文章
# 未删除、未下架的文章
# 频道筛选

# 加载首页  正常查询
# 加载更多  小于最小发布时间
# 加载最新  大于最大发布时间

select *
from ap_article aa
left join ap_article_config aac on aa.id = aac.article_id
where aa.channel_id = 1
and aa.publish_time > '2020-09-07 22:30:09'
and aac.is_down != 1 and aac.is_delete != 1
order by aa.publish_time desc
limit 10
```





### 1.5 接口定义

|          | **加载首页**         | **加载更多**             | **加载最新**            |
| -------- | -------------------- | ------------------------ | ----------------------- |
| 接口路径 | /api/v1/article/load | /api/v1/article/loadmore | /api/v1/article/loadnew |
| 请求方式 | POST                 | POST                     | POST                    |
| 参数     | ArticleHomeDto       | ArticleHomeDto           | ArticleHomeDto          |
| 响应结果 | ResponseResult       | ResponseResult           | ResponseResult          |

响应结果ResponseResult示例：  

```json
{
	"code": 200,
	"errorMessage": "操作成功",
	"data": [
		{
			"id": 1302862387124125697,
			"userId": 1102,
			"title": "什么是Java语言"
		},
		......
	]
}
```



在heima-leadnews-model模块中，添加ArticleHomeDto

```java
package com.heima.model.article.dtos;

import lombok.Data;

import java.util.Date;

@Data
public class ArticleHomeDto {

    // 最大时间
    Date maxBehotTime;
    // 最小时间
    Date minBehotTime;
    //加载类型
    Integer loaddir;
    // 分页size
    Integer size;
    // 频道ID
    String tag;
}
```



### 1.6 功能实现

#### 1.6.1 导入文章微服务

在当天的  资料 \ 文章微服务  目录中找到    heima-leadnews-article.zip

解压到项目中

![image-20210420000326669](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210420000326669.png)

<font color='red'>注意：需要在heima-leadnews-service的pom文件夹中添加子模块信息，如下：</font>

```xml
<modules>
    <module>heima-leadnews-user</module>
    <module>heima-leadnews-article</module>
</modules>
```

在idea中的maven中更新一下，如果工程还是灰色的，需要在重新添加文章微服务的pom文件，操作步骤如下：

![image-20210420001037992](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210420001037992.png)





需要在nacos中添加对应的配置

<img src="app端文章查看，静态化freemarker,分布式文件系统minIO.assets/image-20220425181759656.png" alt="image-20220425181759656" style="zoom:80%;" />

```yaml
spring:
  datasource:
    driver-class-name: com.mysql.cj.jdbc.Driver
    url: jdbc:mysql://localhost:3306/leadnews_article?useUnicode=true&characterEncoding=utf8&autoReconnect=true&allowMultiQueries=true&useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true
    username: root
    password: root
# 设置Mapper接口所对应的XML文件位置，如果你在Mapper接口中有自定义方法，需要进行该配置
mybatis-plus:
  mapper-locations: classpath*:mapper/*.xml
  # 设置别名包扫描路径，通过该属性可以给包中的类注册别名
  type-aliases-package: com.heima.model.article.pojos
```



在nacos的配置中心的leadnews-app-gateway中添加文章微服务的路由，完整配置如下：

```yaml
spring:
  cloud:
    gateway:
      globalcors:
        cors-configurations:
          '[/**]': # 匹配所有请求
            allowedOrigins: "*" #跨域处理 允许所有的域
            allowedMethods: # 支持的方法
              - GET
              - POST
              - PUT
              - DELETE
      routes:
        # 用户微服务
        - id: user
          uri: lb://leadnews-user
          predicates:
            - Path=/user/**
          filters:
            - StripPrefix= 1
        # 文章微服务
        - id: article
          uri: lb://leadnews-article
          predicates:
            - Path=/article/**
          filters:
            - StripPrefix= 1
```



#### 1.6.2 定义接口

```java
package com.heima.article.controller;

import com.heima.article.service.ArticleHomeService;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/article")
public class ArticleHomeController {

    @Autowired
    private ArticleHomeService articleHomeService;

    //加载首页 /load
    @PostMapping({"/load", "/loadmore", "/loadnew"})
    public ResponseResult load(@RequestBody ArticleHomeDto dto) {
        return articleHomeService.load(dto);
    }
}
```



#### 1.6.3 编写业务层代码

```java
package com.heima.article.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;

public interface ArticleHomeService extends IService<ApArticle> {

    //加载文章
    ResponseResult load(ArticleHomeDto dto);
}
```

实现类：

```java
package com.heima.article.service.impl;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.article.service.ArticleHomeService;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.common.dtos.ResponseResult;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class ArticleHomeServiceImpl extends ServiceImpl<ApArticleMapper, ApArticle> implements ArticleHomeService {

    //加载文章
    @Override
    public ResponseResult load(ArticleHomeDto dto) {
        //Page<ApArticle> page = page(new Page<ApArticle>(1, dto.getSize()));
        List<ApArticle> apArticles = baseMapper.loadArticleList(dto);
        return ResponseResult.okResult(apArticles);
    }
}
```



#### 1.6.3 编写mapper文件

```java
package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.dtos.ArticleHomeDto;
import com.heima.model.article.pojos.ApArticle;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ApArticleMapper extends BaseMapper<ApArticle> {

    //加载文章列表
    List<ApArticle> loadArticleList(@Param("dto") ArticleHomeDto dto);
}
```



粘贴对应的映射文件

在resources中新建mapper/ApArticleMapper.xml     如下配置：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="com.heima.article.mapper.ApArticleMapper">


    <select id="loadArticleList" resultType="com.heima.model.article.pojos.ApArticle">
        select aa.* from ap_article aa
        left join ap_article_config aac on aa.id = aac.article_id
        <where>
            aac.is_down!=1 and aac.is_delete!=1
            <if test="dto.loaddir!=null and dto.loaddir==0">
                and publish_time <![CDATA[>]]> #{dto.maxBehotTime}
            </if>
            <if test="dto.loaddir!=null and dto.loaddir==2">
                and publish_time <![CDATA[<]]> #{dto.minBehotTime}
            </if>
            <if test="dto.tag!=null and dto.tag != '__all__'">
                and channel_id = #{dto.tag}
            </if>
        </where>
        order by publish_time desc
        limit #{dto.size}
    </select>
</mapper>
```



## 2 freemarker

文章详情页实现方案：

**方案1：**

<img src="app端文章查看，静态化freemarker,分布式文件系统minIO.assets/image-20220425191804669.png" alt="image-20220425191804669" style="zoom:80%;" />



**方案2：静态模板展示**

<img src="app端文章查看，静态化freemarker,分布式文件系统minIO.assets/image-20220425192020366.png" alt="image-20220425192020366" style="zoom:80%;" />



### 2.1 freemarker 介绍

​	FreeMarker 是一款 模板引擎： 即一种基于模板和要改变的数据， 并用来生成输出文本(HTML网页，电子邮件，配置文件，源代码等)的通用工具。 它不是面向最终用户的，而是一个Java类库，是一款程序员可以嵌入他们所开发产品的组件。

​	模板编写为FreeMarker Template Language (FTL)。它是简单的，专用的语言， *不是* 像PHP那样成熟的编程语言。 那就意味着要准备数据在真实编程语言中来显示，比如数据库查询和业务运算， 之后模板显示已经准备好的数据。在模板中，你可以专注于如何展现数据， 而在模板之外可以专注于要展示什么数据。 

![1528820943975](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\1528820943975.png)

http://freemarker.foofun.cn/toc.html

常用的java模板引擎还有哪些？

| **技术**   | **说明**                                                     |
| ---------- | ------------------------------------------------------------ |
| Jsp        | Jsp  为 Servlet 专用，不能单独进行使用                       |
| Velocity   | Velocity从2010年更新完 2.0  版本后，7年没有更新。Spring Boot 官方在 1.4  版本后对此也不在支持 |
| thmeleaf   | 新技术，功能较为强大，但是执行的效率比较低                   |
| freemarker | 性能好，强大的模板语言、轻量                                 |





### 2.2 环境搭建&快速入门

freemarker作为springmvc一种视图格式，默认情况下SpringMVC支持freemarker视图格式。

需要创建Spring Boot+Freemarker工程用于测试模板。

#### 2.2.1 创建测试工程

创建一个freemarker-demo 的测试工程专门用于freemarker的功能测试与模板的测试。

pom.xml如下

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-freemarker</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
    </dependency>
    <!-- lombok -->
    <dependency>
        <groupId>org.projectlombok</groupId>
        <artifactId>lombok</artifactId>
    </dependency>

    <!-- apache 对 java io 的封装工具库 -->
    <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-io</artifactId>
        <version>1.3.2</version>
    </dependency>
</dependencies>
```



#### 2.2.2 配置文件

配置application.yml ：

```yaml
server:
  port: 8881 #服务端口
spring:
  application:
    name: freemarker-demo #指定服务名
  freemarker:
    cache: false  #关闭模板缓存，方便测试
    settings:
      template_update_delay: 0 #检查模板更新延迟时间，设置为0表示立即检查，如果时间大于0会有缓存不方便进行模板测试
    suffix: .ftl               #指定Freemarker模板文件的后缀名
```



#### 2.2.3 创建模型类

在freemarker的测试工程下创建模型类型用于测试

```java
package com.heima.freemarker.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Student {
    private String name;//姓名
    private int age;//年龄
    private Date birthday;//生日
    private Float money;//钱
}
```



#### 2.2.4 创建controller

创建Controller类，向Map中添加name，最后返回模板文件。

```java
package com.xuecheng.test.freemarker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Controller
public class HelloController {

    @GetMapping("/basic")
    public String test(Model model) {


        //1.纯文本形式的参数
        model.addAttribute("name", "freemarker");
        //2.实体类相关的参数
        
        Student student = new Student();
        student.setName("小明");
        student.setAge(18);
        model.addAttribute("stu", student);

        return "01-basic";
    }
}
```



#### 2.2.5 编写模板

在resources下创建templates，此目录为freemarker的默认模板存放目录。

在templates下创建模板文件 01-basic.ftl ，模板中的插值表达式最终会被freemarker替换成具体的数据。

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Hello World!</title>
</head>
<body>
<b>普通文本 String 展示：</b><br><br>
Hello ${name} <br>
<hr>
<b>对象Student中的数据展示：</b><br/>
姓名：${stu.name}<br/>
年龄：${stu.age}
<hr>
</body>
</html>
```



#### 2.2.6 创建启动类

```java
package com.heima.freemarker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FreemarkerDemotApplication {
    public static void main(String[] args) {
        SpringApplication.run(FreemarkerDemotApplication.class,args);
    }
}
```



#### 2.2.7 测试

请求：http://localhost:8881/basic

![1576129529361](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\1576129529361.png)





### 2.3 freemarker基础

#### 2.3.1 基础语法种类

  1、注释，即<#--  -->，介于其之间的内容会被freemarker忽略

```velocity
<#--我是一个freemarker注释-->
```

  2、插值（Interpolation）：即 **`${..}`** 部分,freemarker会用真实的值代替**`${..}`**

```velocity
Hello ${name}
```

  3、FTL指令：和HTML标记类似，名字前加#予以区分，Freemarker会解析标签中的表达式或逻辑。

```velocity
<# >FTL指令</#> 
```

  4、文本，仅文本信息，这些不是freemarker的注释、插值、FTL指令的内容会被freemarker忽略解析，直接输出内容。

```velocity
<#--freemarker中的普通文本-->
我是一个普通的文本
```



#### 2.3.2 集合指令（List和Map）

1、数据模型：

在HelloController中新增如下方法：

```java
@GetMapping("/list")
public String list(Model model){

    //------------------------------------
    Student stu1 = new Student();
    stu1.setName("小强");
    stu1.setAge(18);
    stu1.setMoney(1000.86f);
    stu1.setBirthday(new Date());

    //小红对象模型数据
    Student stu2 = new Student();
    stu2.setName("小红");
    stu2.setMoney(200.1f);
    stu2.setAge(19);

    //将两个对象模型数据存放到List集合中
    List<Student> stus = new ArrayList<>();
    stus.add(stu1);
    stus.add(stu2);

    //向model中存放List集合数据
    model.addAttribute("stus",stus);

    //------------------------------------

    //创建Map数据
    HashMap<String,Student> stuMap = new HashMap<>();
    stuMap.put("stu1",stu1);
    stuMap.put("stu2",stu2);
    // 3.1 向model中存放Map数据
    model.addAttribute("stuMap", stuMap);

    return "02-list";
}
```

2、模板：

在templates中新增`02-list.ftl`文件

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Hello World!</title>
</head>
<body>
    
<#-- list 数据的展示 -->
<b>展示list中的stu数据:</b>
<br>
<br>
<table>
    <tr>
        <td>序号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>钱包</td>
    </tr>
</table>
<hr>
    
<#-- Map 数据的展示 -->
<b>map数据的展示：</b>
<br/><br/>
<a href="###">方式一：通过map['keyname'].property</a><br/>
输出stu1的学生信息：<br/>
姓名：<br/>
年龄：<br/>
<br/>
<a href="###">方式二：通过map.keyname.property</a><br/>
输出stu2的学生信息：<br/>
姓名：<br/>
年龄：<br/>

<br/>
<a href="###">遍历map中两个学生信息：</a><br/>
<table>
    <tr>
        <td>序号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>钱包</td> 
    </tr>
</table>
<hr>
 
</body>
</html>
```



对上面的基础模板进行补充，能够正常解析集合，补全后的代码如下：

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Hello World!</title>
</head>
<body>
    
<#-- list 数据的展示 -->
<b>展示list中的stu数据:</b>
<br>
<br>
<table>
    <tr>
        <td>序号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>钱包</td>
    </tr>
    <#list stus as stu>
        <tr>
            <td>${stu_index+1}</td>
            <td>${stu.name}</td>
            <td>${stu.age}</td>
            <td>${stu.money}</td>
        </tr>
    </#list>

</table>
<hr>
    
<#-- Map 数据的展示 -->
<b>map数据的展示：</b>
<br/><br/>
<a href="###">方式一：通过map['keyname'].property</a><br/>
输出stu1的学生信息：<br/>
姓名：${stuMap['stu1'].name}<br/>
年龄：${stuMap['stu1'].age}<br/>
<br/>
<a href="###">方式二：通过map.keyname.property</a><br/>
输出stu2的学生信息：<br/>
姓名：${stuMap.stu2.name}<br/>
年龄：${stuMap.stu2.age}<br/>

<br/>
<a href="###">遍历map中两个学生信息：</a><br/>
<table>
    <tr>
        <td>序号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>钱包</td>
    </tr>
    <#list stuMap?keys as key >
        <tr>
            <td>${key_index}</td>
            <td>${stuMap[key].name}</td>
            <td>${stuMap[key].age}</td>
            <td>${stuMap[key].money}</td>
        </tr>
    </#list>
</table>
<hr>
 
</body>
</html>
```

👆上面代码解释：

${k_index}：
	index：得到循环的下标，使用方法是在stu后边加"_index"，它的值是从0开始



#### 2.3.3 if指令

​	 if 指令即判断指令，是常用的FTL指令，freemarker在解析时遇到if会进行判断，条件为真则输出if中间的内容，否则跳过内容不再输出。

- 指令格式

```html
<#if></if>
```



1、数据模型：

使用list指令中测试数据模型，判断名称为小红的数据字体显示为红色。

2、模板：

```velocity
<table>
    <tr>
        <td>姓名</td>
        <td>年龄</td>
        <td>钱包</td>
    </tr>
    <#list stus as stu>
        <tr>
            <td>${stu.name}</td>
            <td>${stu.age}</td>
            <td>${stu.money}</td>
        </tr>
    </#list>

</table>
```



实例代码：

```velocity
<table>
    <tr>
        <td>姓名</td>
        <td>年龄</td>
        <td>钱包</td>
    </tr>
    <#list stus as stu >
        <#if stu.name='小红'>
            <tr style="color: red">
                <td>${stu_index}</td>
                <td>${stu.name}</td>
                <td>${stu.age}</td>
                <td>${stu.money}</td>
            </tr>
            <#else >
            <tr>
                <td>${stu_index}</td>
                <td>${stu.name}</td>
                <td>${stu.age}</td>
                <td>${stu.money}</td>
            </tr>
        </#if>
    </#list>
</table>
```





3、输出：

姓名为“小强”则字体颜色显示为红色。

![1539947776259](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\1539947776259.png)



#### 2.3.4 运算符

**1、算数运算符**

FreeMarker表达式中完全支持算术运算,FreeMarker支持的算术运算符包括:

- 加法： `+`
- 减法： `-`
- 乘法： `*`
- 除法： `/`
- 求模 (求余)： `%`



模板代码

```html
<b>算数运算符</b>
<br/><br/>
    100+5 运算：  ${100 + 5 }<br/>
    100 - 5 * 5运算：${100 - 5 * 5}<br/>
    5 / 2运算：${5 / 2}<br/>
    12 % 10运算：${12 % 10}<br/>
<hr>
```

除了 + 运算以外，其他的运算只能和 number 数字类型的计算。







**2、比较运算符**

- **`=`**或者**`==`**:判断两个值是否相等. 
- **`!=`**:判断两个值是否不等. 
- **`>`**或者**`gt`**:判断左边值是否大于右边值 
- **`>=`**或者**`gte`**:判断左边值是否大于等于右边值 
- **`<`**或者**`lt`**:判断左边值是否小于右边值 
- **`<=`**或者**`lte`**:判断左边值是否小于等于右边值 



Controller 的 数据模型代码

```java
@GetMapping("operation")
public String testOperation(Model model) {
    //构建 Date 数据
    Date now = new Date();
    model.addAttribute("date1", now);
    model.addAttribute("date2", now);
    
    return "03-operation";
}
```



= 和 == 模板代码

创建文件`03-operation.ftl`

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Hello World!</title>
</head>
<body>

    <b>比较运算符</b>
    <br/>
    <br/>

    <dl>
        <dt> =/== 和 != 比较：</dt>
        <dd>
            <#if "xiaoming" == "xiaoming">
                字符串的比较 "xiaoming" == "xiaoming"
            </#if>
        </dd>
        <dd>
            <#if 10 != 100>
                数值的比较 10 != 100
            </#if>
        </dd>
    </dl>



    <dl>
        <dt>其他比较</dt>
        <dd>
            <#if 10 gt 5 >
                形式一：使用特殊字符比较数值 10 gt 5
            </#if>
        </dd>
        <dd>
            <#-- 日期的比较需要通过?date将属性转为data类型才能进行比较 -->
            <#if (date1?date >= date2?date)>
                形式二：使用括号形式比较时间 date1?date >= date2?date
            </#if>
        </dd>
    </dl>

    <br/>
<hr>
</body>
</html>
```



**比较运算符注意**

- **`=`**和**`!=`**可以用于字符串、数值和日期来比较是否相等
- **`=`**和**`!=`**两边必须是相同类型的值,否则会产生错误
- 字符串 **`"x"`** 、**`"x "`** 、**`"X"`**比较是不等的.因为FreeMarker是精确比较
- 其它的运行符可以作用于数字和日期,但不能作用于字符串
- 使用**`gt`**等字母运算符代替**`>`**会有更好的效果,因为 FreeMarker会把**`>`**解释成FTL标签的结束字符
- 可以使用括号来避免这种情况,如:**`<#if (x>y)>`**





**3、逻辑运算符**

- 逻辑与:&& 
- 逻辑或:|| 
- 逻辑非:! 

逻辑运算符只能作用于布尔值,否则将产生错误 。



模板代码

```html
<b>逻辑运算符</b>
    <br/>
    <br/>
    <#if (10 lt 12 )&&( 10  gt  5 )  >
        (10 lt 12 )&&( 10  gt  5 )  显示为 true
    </#if>
    <br/>
    <br/>
    <#if !false>
        false 取反为true
    </#if>
<hr>
```



#### 2.3.5 空值处理

**1、判断某变量是否存在使用 “??”**

用法为:variable??,如果该变量存在,返回true,否则返回false 

例：为防止stus为空报错可以加上判断如下：

```velocity
    <#if stus??>
    <#list stus as stu>
    	......
    </#list>
    </#if>
```



**2、缺失变量默认值使用 “!”**

- 使用!要以指定一个默认值，当变量为空时显示默认值

  例：  ${name!''}表示如果name为空显示空字符串。



- 如果是嵌套对象则建议使用（）括起来

  例： ${(stu.bestFriend.name)!''}表示，如果stu或bestFriend或name为空默认显示空字符串。





#### 2.3.6 内建函数

内建函数语法格式： **`变量+?+函数名称`**  

**1、和到某个集合的大小**

**`${集合名?size}`**



**2、日期格式化**

显示年月日: **`${today?date}`** 
显示时分秒：**`${today?time}`**   
显示日期+时间：**`${today?datetime}`**   
自定义格式化：  **`${today?string("yyyy年MM月")}`**



**3、内建函数`c`**

model.addAttribute("point", 102920122);

point是数字型，使用${point}会显示这个数字的值，每三位使用逗号分隔。

如果不想显示为每三位分隔的数字，可以使用c函数将数字型转成字符串输出

**`${point?c}`**



**4、将json字符串转成对象**

一个例子：

其中用到了 assign标签，assign的作用是定义一个变量。

```velocity
<#assign text="{'bank':'工商银行','account':'10101920201920212'}" />
<#assign data=text?eval />
开户行：${data.bank}  账号：${data.account}
```



内建函数Controller数据模型：

```java
@GetMapping("innerFunc")
public String testInnerFunc(Model model) {
    //1.1 小强对象模型数据
    Student stu1 = new Student();
    stu1.setName("小强");
    stu1.setAge(18);
    stu1.setMoney(1000.86f);
    stu1.setBirthday(new Date());
    //1.2 小红对象模型数据
    Student stu2 = new Student();
    stu2.setName("小红");
    stu2.setMoney(200.1f);
    stu2.setAge(19);
    //1.3 将两个对象模型数据存放到List集合中
    List<Student> stus = new ArrayList<>();
    stus.add(stu1);
    stus.add(stu2);
    model.addAttribute("stus", stus);
    // 2.1 添加日期
    Date date = new Date();
    model.addAttribute("today", date);
    // 3.1 添加数值
    model.addAttribute("point", 102920122);
    return "04-innerFunc";
}
```





模板代码：

创建文件  `04-innerFunc.ftl`

````HTML
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>inner Function</title>
</head>
<body>

    <b>获得集合大小</b><br>

    集合大小：
    <hr>


    <b>获得日期</b><br>

    显示年月日:      <br>

    显示时分秒：<br>

    显示日期+时间：<br>

    自定义格式化：  <br>

    <hr>

    <b>内建函数C</b><br>
    没有C函数显示的数值： <br>

    有C函数显示的数值：

    <hr>

    <b>声明变量assign</b><br>


<hr>
</body>
</html>
````





内建函数模板页面：

```velocity
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>inner Function</title>
</head>
<body>

    <b>获得集合大小</b><br>

    集合大小：${stus?size}
    <hr>


    <b>获得日期</b><br>

    显示年月日: ${today?date}       <br>

    显示时分秒：${today?time}<br>

    显示日期+时间：${today?datetime}<br>

    自定义格式化：  ${today?string("yyyy年MM月")}<br>

    <hr>

    <b>内建函数C</b><br>
    没有C函数显示的数值：${point} <br>

    有C函数显示的数值：${point?c}

    <hr>

    <b>声明变量assign</b><br>
    <#assign text="{'bank':'工商银行','account':'10101920201920212'}" />
    <#assign data=text?eval />
    开户行：${data.bank}  账号：${data.account}

<hr>
</body>
</html>
```





### 2.4 静态化测试

之前的测试都是SpringMVC将Freemarker作为视图解析器（ViewReporter）来集成到项目中，工作中，有的时候需要使用Freemarker原生Api来生成静态内容，下面一起来学习下原生Api生成文本文件。

#### 2.4.1 需求分析

使用freemarker原生Api将页面生成html文件，本节测试html文件生成的方法：

![image-20210422163843108](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210422163843108.png)

#### 2.4.2 静态化测试 

根据模板文件生成html文件

①：修改application.yml文件，添加以下模板存放位置的配置信息，完整配置如下：

```yaml
server:
  port: 8881 #服务端口
spring:
  application:
    name: freemarker-demo #指定服务名
  freemarker:
    cache: false  #关闭模板缓存，方便测试
    settings:
      template_update_delay: 0 #检查模板更新延迟时间，设置为0表示立即检查，如果时间大于0会有缓存不方便进行模板测试
    suffix: .ftl               #指定Freemarker模板文件的后缀名
    template-loader-path: classpath:/templates   #模板存放位置
```

②：在test下创建测试类

```java
package com.heima.freemarker.test;


import com.heima.freemarker.FreemarkerDemoApplication;
import com.heima.freemarker.entity.Student;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

@SpringBootTest(classes = FreemarkerDemoApplication.class)
public class FreemarkerTest {

    @Autowired
    private Configuration configuration;

    @Test
    public void test() throws IOException, TemplateException {
        //freemarker的模板对象，获取模板
        Template template = configuration.getTemplate("02-list.ftl");
        Map params = getData();
        //合成
        //第一个参数 数据模型
        //第二个参数  输出流
        template.process(params, new FileWriter("C:\\5_code\\list.html"));
    }

    private Map getData() {
        Map<String, Object> map = new HashMap<>();

        //小强对象模型数据
        Student stu1 = new Student();
        stu1.setName("小强");
        stu1.setAge(18);
        stu1.setMoney(1000.86f);
        stu1.setBirthday(new Date());

        //小红对象模型数据
        Student stu2 = new Student();
        stu2.setName("小红");
        stu2.setMoney(200.1f);
        stu2.setAge(19);

        //将两个对象模型数据存放到List集合中
        List<Student> stus = new ArrayList<>();
        stus.add(stu1);
        stus.add(stu2);

        //向map中存放List集合数据
        map.put("stus", stus);


        //创建Map数据
        HashMap<String, Student> stuMap = new HashMap<>();
        stuMap.put("stu1", stu1);
        stuMap.put("stu2", stu2);
        //向map中存放Map数据
        map.put("stuMap", stuMap);

        //返回Map
        return map;
    }
}
```



## 3 对象存储服务MinIO 

常见的对象存储服务

| 存储方式       | 优点                       | 缺点     |
| -------------- | -------------------------- | -------- |
| 服务器磁盘     | 开发便捷，成本低           | 扩展困难 |
| 分布式文件系统 | 容易实现扩容               | 复杂度高 |
| 第三方存储     | 开发简单，功能强大，免维护 | 收费     |



分布式文件系统

| 存储方式 | 优点                                                         | 缺点                                                 |
| :------: | ------------------------------------------------------------ | ---------------------------------------------------- |
| FastDFS  | 1，主备服务，高可用  <br />2，支持主从文件，支持自定义扩展名  <br />3，支持动态扩容 | 1，官方文档近几年没有更新  <br />2，环境搭建较为麻烦 |
|  MinIO   | 1，性能高，读/写速度最高达到183 GB/s和171  GB/s。  <br />2，部署自带管理界面  <br />3，MinIO.Inc运营的开源项目，社区活跃度高  <br />4，提供了所有主流开发语言的SDK | 不支持动态增加节点                                   |



### 3.1 MinIO简介   

MinIO基于Apache License v2.0开源协议的对象存储服务，可以做为云存储的解决方案用来保存海量的图片，视频，文档。由于采用Golang实现，服务端可以工作在Windows,Linux, OS X和FreeBSD上。配置简单，基本是复制可执行程序，单行命令可以运行起来。

MinIO兼容亚马逊S3云存储服务接口，非常适合于存储大容量非结构化的数据，例如图片、视频、日志文件、备份数据和容器/虚拟机镜像等，而一个对象文件可以是任意大小，从几kb到最大5T不等。

**S3 （ Simple Storage Service简单存储服务）**

基本概念

- bucket – 类比于文件系统的目录
- Object – 类比文件系统的文件
- Keys – 类比文件名

官网文档：http://docs.minio.org.cn/docs/

### 3.2 MinIO特点 

- 数据保护

  Minio使用Minio Erasure Code（纠删码）来防止硬件故障。即便损坏一半以上的driver，但是仍然可以从中恢复。

- 高性能

  作为高性能对象存储，在标准硬件条件下它能达到55GB/s的读、35GB/s的写速率

- 可扩容

  不同MinIO集群可以组成联邦，并形成一个全局的命名空间，并跨越多个数据中心

- SDK支持

  基于Minio轻量的特点，它得到类似Java、Python或Go等语言的sdk支持

- 有操作页面

  面向用户友好的简单操作界面，非常方便的管理Bucket及里面的文件资源

- 功能简单

  这一设计原则让MinIO不容易出错、更快启动

- 丰富的API

  支持文件资源的分享连接及分享链接的过期策略、存储桶操作、文件列表访问及文件上传下载的基本功能等。

- 文件变化主动通知

  存储桶（Bucket）如果发生改变,比如上传对象和删除对象，可以使用存储桶事件通知机制进行监控，并通过以下方式发布出去:AMQP、MQTT、Elasticsearch、Redis、NATS、MySQL、Kafka、Webhooks等。


### 3.3 开箱使用 

#### 3.3.1 安装启动   

我们提供的镜像中已经有minio的环境

我们可以使用docker进行环境部署和启动

```yaml
docker run -id --name minio \
-v /opt/minio/data:/data \
-v /opt/minio/config:/root/.minio \
--restart=always -p 9000:9000 -p 9001:9001 \
-e "MINIO_ACCESS_KEY=minio" -e "MINIO_SECRET_KEY=minio123" \
minio/minio:RELEASE.2022-04-16T04-26-02Z \
server /data --console-address ":9001"
```



#### 3.3.2 管理控制台   

在地址栏输入：http://192.168.200.128:9001/ 即可进入登录界面。

用户名为minio   密码为minio123

<img src="app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210417102204739.png" alt="image-20210417102204739" style="zoom:80%;" />



主界面创建Bucket桶

![image-20220425233907585](app端文章查看，静态化freemarker,分布式文件系统minIO.assets/image-20220425233907585.png)



创建桶

<img src="app端文章查看，静态化freemarker,分布式文件系统minIO.assets/image-20220425234023190.png" alt="image-20220425234023190" style="zoom:80%;" />

设置桶的访问策略为开放

<img src="app端文章查看，静态化freemarker,分布式文件系统minIO.assets/image-20220425234330993.png" alt="image-20220425234330993" style="zoom:80%;" />

<img src="app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210417102356582.png" alt="image-20210417102356582" style="zoom:80%;" />





### 3.4 快速入门

#### 3.4.1 创建工程，导入pom依赖

创建minio-demo,对应pom如下

```xml
<dependencies>
    <dependency>
        <groupId>io.minio</groupId>
        <artifactId>minio</artifactId>
        <version>7.1.0</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
    </dependency>

    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-test</artifactId>
    </dependency>
</dependencies>
```

引导类：

```java
package com.heima.minio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class MinIOApplication {

    public static void main(String[] args) {
        SpringApplication.run(MinIOApplication.class,args);
    }
}
```

创建测试类，上传html文件

```java
package com.heima.minio.test;

import io.minio.MinioClient;
import io.minio.PutObjectArgs;

import java.io.FileInputStream;

public class MinIOTest {

    public static void main(String[] args) {

        FileInputStream fileInputStream = null;
        try {

            fileInputStream =  new FileInputStream("C:\\5_code\\list.html");;

            //1.创建minio链接客户端
            MinioClient minioClient = MinioClient.builder()
                    .credentials("minio", "minio123")
                    .endpoint("http://192.168.200.128:9000").build();
            //2.上传
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .object("list.html")//文件名
                    .contentType("text/html")//文件类型
                    .bucket("leadnews")//桶名词  与minio创建的名词一致
                    .stream(fileInputStream, fileInputStream.available(), -1) //文件流
                    .build();
            minioClient.putObject(putObjectArgs);

            System.out.println("http://192.168.200.128:9000/leadnews/list.html");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
```



### 3.5 封装MinIO模块

使用SpringBoot的自动装配，把MinIO的Client进行封装，其他的微服务方便进行使用

#### 3.5.1 添加依赖

在**heima-leadnews-common**中添加依赖

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-autoconfigure</artifactId>
</dependency>
<dependency>
    <groupId>io.minio</groupId>
    <artifactId>minio</artifactId>
    <version>7.1.0</version>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter</artifactId>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-configuration-processor</artifactId>
    <optional>true</optional>
</dependency>
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```



#### 3.5.2 配置类

在**heima-leadnews-common**中编写MinIOProperties，用来读取MinIO的配置信息

```java
package com.heima.common.autoconfig.properties;


import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "minio")  // 配置前缀
public class MinIOProperties {

    private String accessKey;
    private String secretKey;
    private String bucket;
    private String endpoint;
    private String readPath;
}
```



#### 3.5.3 封装操作minIO类

在**heima-leadnews-common**中编写MinIOTemplate，用来操作MinIO的工具类

```java
package com.heima.common.autoconfig.template;


import com.heima.common.autoconfig.properties.MinIOProperties;
import io.minio.GetObjectArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class MinIOTemplate {

    private MinioClient minioClient;
    private MinIOProperties minIOProperties;

    public MinIOTemplate(MinIOProperties minIOProperties) {
        this.minIOProperties = minIOProperties;
        this.minioClient = MinioClient
                .builder()
                .credentials(minIOProperties.getAccessKey(), minIOProperties.getSecretKey())
                .endpoint(minIOProperties.getEndpoint())
                .build();
    }


    private final static String separator = "/";

    /**
     * @param dirPath
     * @param filename yyyy/mm/dd/file.jpg
     * @return
     */
    public String builderFilePath(String dirPath, String filename) {
        StringBuilder stringBuilder = new StringBuilder(50);
        if (!StringUtils.isEmpty(dirPath)) {
            stringBuilder.append(dirPath).append(separator);
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String todayStr = sdf.format(new Date());
        stringBuilder.append(todayStr).append(separator);
        stringBuilder.append(filename);
        return stringBuilder.toString();
    }

    /**
     * 上传图片文件
     *
     * @param prefix      文件前缀
     * @param filename    文件名
     * @param inputStream 文件流
     * @return 文件全路径
     */
    public String uploadImgFile(String prefix, String filename, InputStream inputStream) {
        String filePath = builderFilePath(prefix, filename);
        try {
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .object(filePath)
                    .contentType("image/jpg")
                    .bucket(minIOProperties.getBucket()).stream(inputStream, inputStream.available(), -1)
                    .build();
            minioClient.putObject(putObjectArgs);
            StringBuilder urlPath = new StringBuilder(minIOProperties.getReadPath());
            urlPath.append(separator + minIOProperties.getBucket());
            urlPath.append(separator);
            urlPath.append(filePath);
            return urlPath.toString();
        } catch (Exception ex) {
            log.error("minio put file error.", ex);
            throw new RuntimeException("上传文件失败");
        }
    }

    /**
     * 上传html文件
     *
     * @param prefix      文件前缀
     * @param filename    文件名
     * @param inputStream 文件流
     * @return 文件全路径
     */
    public String uploadHtmlFile(String prefix, String filename, InputStream inputStream) {
        String filePath = builderFilePath(prefix, filename);
        try {
            PutObjectArgs putObjectArgs = PutObjectArgs.builder()
                    .object(filePath)
                    .contentType("text/html")
                    .bucket(minIOProperties.getBucket()).stream(inputStream, inputStream.available(), -1)
                    .build();
            minioClient.putObject(putObjectArgs);
            StringBuilder urlPath = new StringBuilder(minIOProperties.getReadPath());
            urlPath.append(separator + minIOProperties.getBucket());
            urlPath.append(separator);
            urlPath.append(filePath);
            return urlPath.toString();
        } catch (Exception ex) {
            log.error("minio put file error.", ex);
            ex.printStackTrace();
            throw new RuntimeException("上传文件失败");
        }
    }

    /**
     * 删除文件
     *
     * @param pathUrl 文件全路径
     */
    public void delete(String pathUrl) {
        String key = pathUrl.replace(minIOProperties.getEndpoint() + "/", "");
        int index = key.indexOf(separator);
        String bucket = key.substring(0, index);
        String filePath = key.substring(index + 1);
        // 删除Objects
        RemoveObjectArgs removeObjectArgs = RemoveObjectArgs.builder().bucket(bucket).object(filePath).build();
        try {
            minioClient.removeObject(removeObjectArgs);
        } catch (Exception e) {
            log.error("minio remove file error.  pathUrl:{}", pathUrl);
            e.printStackTrace();
        }
    }


    /**
     * 下载文件
     *
     * @param pathUrl 文件全路径
     * @return 文件流
     */
    public byte[] downLoadFile(String pathUrl) {
        String key = pathUrl.replace(minIOProperties.getEndpoint() + "/", "");
        int index = key.indexOf(separator);
        String bucket = key.substring(0, index);
        String filePath = key.substring(index + 1);
        InputStream inputStream = null;
        try {
            inputStream = minioClient.getObject(GetObjectArgs.builder().bucket(minIOProperties.getBucket()).object(filePath).build());
        } catch (Exception e) {
            log.error("minio down file error.  pathUrl:{}", pathUrl);
            e.printStackTrace();
        }

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buff = new byte[100];
        int rc = 0;
        while (true) {
            try {
                if (!((rc = inputStream.read(buff, 0, 100)) > 0)) break;
            } catch (IOException e) {
                e.printStackTrace();
            }
            byteArrayOutputStream.write(buff, 0, rc);
        }
        return byteArrayOutputStream.toByteArray();
    }
}
```



#### 3.5.4 对外加入自动配置

在**heima-leadnews-common**中编写LeadNewsAutoConfig，用来创建对象

```java
package com.heima.common.autoconfig;

import com.heima.common.autoconfig.properties.MinIOProperties;
import com.heima.common.autoconfig.template.MinIOTemplate;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;


@EnableConfigurationProperties({MinIOProperties.class})
public class LeadNewsAutoConfig {

    @Bean
    //如果配置文件中有  minio.enable属性，且值为true，代表该方法会执行，否则不会执行
    @ConditionalOnProperty(prefix = "minio", value = "enable", havingValue = "true")
    public MinIOTemplate minIOTemplate(MinIOProperties minIOProperties) {
        return new MinIOTemplate( minIOProperties);
    }
}
```



在resources中修改`META-INF/spring.factories`

```properties
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
  com.heima.common.exception.ExceptionCatch,\
  com.heima.common.swagger.SwaggerConfiguration,\
  com.heima.common.swagger.Swagger2Configuration,\
  com.heima.common.autoconfig.LeadNewsAutoConfig
```



#### 3.5.5 其他微服务使用

第一，在微服务中添加minio所需要的配置(例如**heima-leadnews-article**)

```yaml
minio:
  accessKey: minio
  secretKey: minio123
  bucket: leadnews
  endpoint: http://192.168.200.128:9000
  readPath: http://192.168.200.128:9000
  enable: true #开启使用Minio 对应LeadNewsAutoConfig中的@ConditionalOnProperty注解属性
```



第二，在对应使用的业务类中注入MinIOTemplate，样例如下：

在**heima-leadnews-article**中进行测试

```java
package com.heima.article.test;


import com.heima.article.ArticleApplication;
import com.heima.common.autoconfig.template.MinIOTemplate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

@SpringBootTest(classes = ArticleApplication.class)
public class MinioTest {

    @Autowired
    private MinIOTemplate minIOTemplate;

    @Test
    public void testUpdateImgFile() {
        try {
            FileInputStream fileInputStream = new FileInputStream("C:\\Users\\Tree-node2020\\Downloads\\d1cefe2e-f67c-4735-bc1c-0bedf14cd992.jpg");
            String filePath = minIOTemplate.uploadImgFile("", "tree1.jpg", fileInputStream);
            System.out.println(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
```



## 4 文章详情

### 4.1)需求分析

![image-20210602180753705](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210602180753705.png)



### 4.2 实现方案

**方案2：静态模板展示**

<img src="app端文章查看，静态化freemarker,分布式文件系统minIO.assets/image-20220425192020366.png" alt="image-20220425192020366" style="zoom:80%;" />



### 4.3 实现步骤

1.在artile微服务中添加MinIO和freemarker的支持，参考测试项目

2.资料中找到模板文件（article.ftl）拷贝到article微服务下

![image-20210602180931839](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210602180931839.png)

3.资料中找到plugins文件夹，手动上传到MinIO中

![image-20210602180957787](app端文章查看，静态化freemarker,分布式文件系统minIO.assets\image-20210602180957787.png)

4.在文章微服务中导入依赖

```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-freemarker</artifactId>
    </dependency>
</dependencies>
```

5.新建ApArticleContentMapper

```java
package com.heima.article.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.heima.model.article.pojos.ApArticleContent;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ApArticleContentMapper extends BaseMapper<ApArticleContent> {
}
```

6.在artile微服务中新增测试类（后期新增文章的时候创建详情静态页，目前暂时手动生成）

```java
package com.heima.article.test;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.heima.article.ArticleApplication;
import com.heima.article.mapper.ApArticleContentMapper;
import com.heima.article.mapper.ApArticleMapper;
import com.heima.common.autoconfig.template.MinIOTemplate;
import com.heima.model.article.pojos.ApArticle;
import com.heima.model.article.pojos.ApArticleContent;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ArticleApplication.class)
public class ArticleFreemarkerTest {

    @Autowired
    private Configuration configuration;
    @Autowired
    private MinIOTemplate minIOTemplate;
    @Autowired
    private ApArticleMapper apArticleMapper;
    @Autowired
    private ApArticleContentMapper apArticleContentMapper;

    @Test
    public void test() throws Exception {
        //获取文本内容
        ApArticleContent content = apArticleContentMapper.selectOne(Wrappers.lambdaQuery(ApArticleContent.class)
                .eq(ApArticleContent::getArticleId, 1302862387124125698L));

        if (content != null && StringUtils.isNotBlank(content.getContent())) {
            //生成html文件
            Map<String, Object> params = new HashMap<>();
            params.put("content", JSONArray.parse(content.getContent()));

            StringWriter out = new StringWriter();
            Template template = configuration.getTemplate("article.ftl");
            template.process(params, out);

            //上传MinIO
            InputStream inputStream = new ByteArrayInputStream(out.toString().getBytes());

            String url = minIOTemplate.uploadHtmlFile("",
                    content.getArticleId() + ".html", inputStream);

            System.out.println(url);

            //更新article
            ApArticle article = new ApArticle();
            article.setId(content.getArticleId());
            article.setStaticUrl(url);
            apArticleMapper.updateById(article);
        }
    }
}
```



